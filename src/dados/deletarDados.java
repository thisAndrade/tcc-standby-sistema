/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import conexao.db_Conexao;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author adria
 */
public class deletarDados extends db_Conexao {

    Connection conn = null;
    ResultSet rs = null;

    public void deletarServicos(int id) {
        try {
            conn = getConnection();

            PreparedStatement create = conn.prepareStatement("delete from tb_servicos where sv_id = ?");
            create.setInt(1, id);
            create.executeUpdate();
            create.close();
            conn.close();
            JOptionPane.showMessageDialog(null, "Servico removido com sucesso!", "SUCESSO!", JOptionPane.INFORMATION_MESSAGE);
        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Erro ao remover o servico.\n\nCOD: " + e + "", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void deletarClientes(int id) throws SQLException {
        try {
            conn = getConnection();

            PreparedStatement create = conn.prepareStatement("delete from tb_clientes where cl_id = ?");
            create.setInt(1, id);

            create.executeUpdate();
            create.close();

        } catch (Exception e) {
        } finally {
            PreparedStatement ifExists = conn.prepareStatement("select * from tb_clientes where cl_id = ?");
            ifExists.setInt(1, id);

            rs = ifExists.executeQuery();
            rs.first();
            if (rs.first() == true) {
                JOptionPane.showMessageDialog(null, "Existem servicos cadastrados no banco\nem nome deste cliente, nao e possivel remove-lo\n\n\nConclua ou delete o servico primeiro.", "Nao foi possivel apagar", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "Cliente deletado com sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            }

            ifExists.close();
            conn.close();
        }
    }

    public void deletarOrcamento(int id) throws SQLException {
        try {
            conn = getConnection();

            PreparedStatement create = conn.prepareStatement("delete from tb_orcamento where orc_id = ?");
            create.setInt(1, id);

            create.executeUpdate();

            create.close();
            conn.close();
        } catch (Exception e) {
        }
    }

    public void deletarProduto(int id) throws SQLException {
        try {
            conn = getConnection();

            PreparedStatement create = conn.prepareStatement("delete from tb_gastos where gst_id = ?");
            create.setInt(1, id);
            
            create.executeUpdate();
            create.close();
            conn.close();
            
        } catch (Exception e) {
            
        }
    }
}
