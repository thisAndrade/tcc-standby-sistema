package dados;

import conexao.db_Conexao;
import java.math.BigDecimal;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 *
 * @author adria
 */
public class alterarDados extends db_Conexao{
    
    public void alterarClientes(int id, String nome, String telefone, String cpf) throws SQLException, ClassNotFoundException{
        Connection conn;
//        try {
            
            conn = getConnection();
            
            PreparedStatement create = conn.prepareStatement("update tb_clientes set cl_nome = ?, cl_telefone = ?, cl_cpf = ? where cl_id = ?");
            create.setString(1, nome);
            create.setString(2, telefone);
            create.setString(3, cpf);
            create.setInt(4, id);
            
            create.executeUpdate();
            create.close();
            conn.close();
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel alterar o cliente.\n\nERRO: "+e+"","ERRO",JOptionPane.ERROR_MESSAGE);
//        }
    }
    
    public void alterarServicos(Date data, String aparelho, String defeito, String situacao, BigDecimal valor_servico, BigDecimal valor_peca, int id, String senha, BigDecimal lucro ) throws SQLException, ClassNotFoundException{
        Connection conn;
        try {
            conn = getConnection();
            PreparedStatement create = conn.prepareStatement("update tb_servicos set sv_data = ?, sv_aparelho = ?, sv_defeito =  ?, sv_situacao = ?, sv_valorservico = ?, sv_valorpeca = ?, sv_senha = ?, sv_lucro = ? where sv_id = ?");
            create.setDate(1, new java.sql.Date(data.getTime()));
            create.setString(2, aparelho);
            create.setString(3, defeito);
            create.setString(4, situacao);
            create.setBigDecimal(5, valor_servico);
            create.setBigDecimal(6, valor_peca);
            create.setString(7, senha);
            create.setBigDecimal(8, lucro);
            create.setInt(9, id);
            
            create.executeUpdate();
            //System.out.println("Tudo certo");
            
        } catch (Exception e) {
        }
    }
    
    public void concluirServicos(Date data, String aparelho, String defeito, String situacao, BigDecimal valor_servico, BigDecimal valor_peca, int id, String senha, BigDecimal lucro, String servico) throws SQLException, ClassNotFoundException{
        Connection conn;
        try {
            conn = getConnection();
            PreparedStatement create = conn.prepareStatement("update tb_servicos set sv_data = ?, sv_aparelho = ?, sv_defeito =  ?, sv_situacao = ?, sv_valorservico = ?, sv_valorpeca = ?, sv_senha = ?, sv_lucro = ?, sv_servico = ?, sv_status = 0 where sv_id = ?");
            create.setDate(1, new java.sql.Date(data.getTime()));
            create.setString(2, aparelho);
            create.setString(3, defeito);
            create.setString(4, situacao);
            create.setBigDecimal(5, valor_servico);
            create.setBigDecimal(6, valor_peca);
            create.setString(7, senha);
            create.setBigDecimal(8, lucro);
            create.setString(9, servico);
            create.setInt(10, id);
            
            create.executeUpdate();
            //System.out.println("Tudo certo");
            
        } catch (Exception e) {
        }
    }
    
    public void desconcluirServicos(int id){
        Connection conn;
        try {
            conn = getConnection();
            PreparedStatement create = conn.prepareStatement("update tb_servicos set sv_status = 1 where sv_id = ?");
            create.setInt(1, id);
            
            create.executeUpdate();
            //System.out.println("Tudo certo");
            
        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Erro ao devolver o servico para as Ordens de Servico\n\nERRO: "+e+"", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void alterarGastoTemporario(int id){
        Connection conn;
        try {
            conn = getConnection();
            PreparedStatement create = conn.prepareStatement("update tb_gastos set gst_temporario = 0 where gst_id = ?");
            create.setInt(1, id);
            create.executeUpdate();
            
            create.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Erro ao transferir gasto temporario para a outra tabela\n\nERRO: "+e+"", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void reiniciarLucrosMes(){
        Connection conn;
        try {
            conn = getConnection();
            PreparedStatement create1 = conn.prepareStatement("update tb_gastos set gst_ativo = 0 where gst_temporario = 0 AND gst_ativo = 1");
            create1.executeUpdate();
            PreparedStatement create2 = conn.prepareStatement("update tb_servicos set sv_ativo = 0 where sv_status = 0 AND sv_ativo = 1");
            create2.executeUpdate();
            
            create1.close();
            create2.close();
            conn.close();
        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Erro ao reiniciar o mes! reinicie o banco de dados (xampp)\n\nERRO: "+e+"", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }
}
