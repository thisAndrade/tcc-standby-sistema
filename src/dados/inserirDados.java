/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import conexao.db_Conexao;
import java.math.BigDecimal;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
/**
 *
 * @author adria
 */
public class inserirDados extends db_Conexao{
    Connection conn = null;
    LocalDateTime dataHoje = LocalDateTime.now();
    DateTimeFormatter dateFormater = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    
    public void inserirCliente(String nome, String telefone, String cpf) throws SQLException, ClassNotFoundException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("INSERT INTO `tb_clientes`(`cl_nome`, `cl_telefone`, `cl_cpf`) VALUES (?, ?, ?)");
        create.setString(1, nome);
        create.setString(2, telefone);
        create.setString(3, cpf);
        
        create.executeUpdate();
        conn.close();
    }
    
    public void inserirServico(int idCliente, String aparelho, String defeito, String situacao, String senha) throws SQLException, ClassNotFoundException{
        
        String data = dateFormater.format(dataHoje); 
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("INSERT  INTO tb_servicos (sv_cl_idcliente, sv_aparelho, sv_defeito, "
                                                        + "sv_situacao, sv_senha, sv_data) "
                                                        + "VALUES (?, ?, ?, ?, ?, ?)");
        create.setInt(1, idCliente);
        create.setString(2, aparelho);
        create.setString(3, defeito);
        create.setString(4, situacao);
        create.setString(5, senha);
        create.setString(6, data);
        
        create.executeUpdate();
        conn.close();
    }
    
    public void inserirOrcamento(String aparelho, String modelo, BigDecimal servico, BigDecimal peca) throws SQLException, ClassNotFoundException{
        
        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("INSERT INTO `tb_orcamento`(`orc_aparelho`, `orc_modelo`, `orc_valor`, `orc_peca`) VALUES (?,?,?,?)");
        
        create.setString(1, aparelho);
        create.setString(2, modelo);
        create.setBigDecimal(3, servico);
        create.setBigDecimal(4, peca);
        
        create.executeUpdate();
        conn.close();
    }
    
    public void inserirProduto(String produto, BigDecimal valor, int temporario) throws SQLException, ClassNotFoundException{
        String data = dateFormater.format(dataHoje); 
        conn = getConnection();
        
        PreparedStatement create = conn.prepareStatement("INSERT INTO tb_gastos (gst_data, gst_produto, gst_valor, gst_temporario) VALUES (?, ?, ?, ?)");
        create.setString(1, data);
        create.setString(2, produto);
        create.setBigDecimal(3, valor);
        create.setInt(4, temporario);
        
        create.executeUpdate();
        
        create.close();
        conn.close();
    }
}
