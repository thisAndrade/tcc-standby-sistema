/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import GUI.j_Principal;
import conexao.db_Conexao;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import modeltable.model;

/**
 *
 * @author AngeL
 */
public class buscarDados extends db_Conexao {

    Connection conn = null;
    ResultSet rs = null;

    public int verificarDuplicidadeCPF(String cpf) {
        try {
            conn = getConnection();

            PreparedStatement create = conn.prepareStatement("select * from tb_clientes where cl_cpf = ?");
            create.setString(1, cpf);
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {
                //System.out.println("existente");
                JOptionPane.showMessageDialog(null, "Ja existe um cliente cadastrado com esse CPF", "CPF Existente", JOptionPane.INFORMATION_MESSAGE);
                return 1;
            }
        } catch (Exception e) {
        }

        return 0;
    }

    public int buscarIdCliente(String nome) throws SQLException, ClassNotFoundException {

        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("select cl_id from tb_clientes where cl_nome = ?");
        create.setString(1, nome);

        rs = create.executeQuery();
        rs.first();

        int id = Integer.parseInt(rs.getObject(1).toString());
        return id;
    }

    public int buscarIdServico(int idCliente) throws SQLException, ClassNotFoundException {

        conn = getConnection();
        PreparedStatement create = conn.prepareStatement("select sv_id from tb_servicos where sv_cl_idcliente = ?");
        create.setInt(1, idCliente);

        rs = create.executeQuery();
        rs.first();

        int idServico = Integer.parseInt(rs.getObject(1).toString());
        return idServico;
    }

    public BigDecimal buscarGastosPessoaisTotais() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Data", "Produto", "Valor"};
        BigDecimal gastosTotais = new BigDecimal(0);
        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select * from tb_gastos where gst_temporario = 0");
            rs = create.executeQuery();
            rs.first();

            do {
                BigDecimal valor = rs.getBigDecimal("gst_valor");

                gastosTotais = gastosTotais.add(valor);
                dados.add(new Object[]{rs.getInt("gst_id"), rs.getDate("gst_data"), rs.getString("gst_produto"), rs.getBigDecimal("gst_valor")});
            } while (rs.next());

        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de gastos pessoais\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return gastosTotais;
    }

    public BigDecimal buscarLucrosServicosProntos() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"Data", "Nome", "Aparelho", "Defeito", "Servico", "Senha", "Peça R$", "Servico R$", "Lucro R$", "Status", "ID"};
        BigDecimal lucrosTotais = new BigDecimal(0);
        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select cl_nome, sv_aparelho, sv_defeito, sv_servico, sv_senha, sv_data, sv_valorpeca,\n"
                            + "sv_lucro, sv_status, sv_id, sv_valorservico from tb_servicos inner join tb_clientes on tb_clientes.cl_id = tb_servicos.sv_cl_idcliente where sv_status = 0");
            rs = create.executeQuery();
            rs.first();

            do {
                BigDecimal lucros = rs.getBigDecimal("sv_lucro");
                lucrosTotais = lucrosTotais.add(lucros);
                dados.add(new Object[]{rs.getDate("sv_data"), rs.getString("cl_nome"), rs.getString("sv_aparelho"), rs.getString("sv_defeito"), rs.getString("sv_servico"),
                    rs.getString("sv_senha"), rs.getBigDecimal("sv_valorpeca"), rs.getBigDecimal("sv_valorservico"), rs.getBigDecimal("sv_lucro"), rs.getInt("sv_status"), rs.getInt("sv_id")});
            } while (rs.next());
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de lucros\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return lucrosTotais;
    }

    public BigDecimal buscarLucrosExtrato(String dataInicial, String dataFinal) {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"Data", "Nome", "Aparelho", "Defeito", "Servico", "Senha", "Peça R$", "Servico R$", "Lucro R$", "Status", "ID"};
        BigDecimal lucrosTotais = new BigDecimal(0);
        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select cl_nome, sv_aparelho, sv_defeito, sv_servico, sv_senha, sv_data, sv_valorpeca,\n"
                            + "sv_lucro, sv_status, sv_id, sv_valorservico from tb_servicos inner join tb_clientes on tb_clientes.cl_id = tb_servicos.sv_cl_idcliente WHERE sv_data BETWEEN (?) AND (?) AND sv_status = 0");
            create.setString(1, dataInicial);
            create.setString(2, dataFinal);

            rs = create.executeQuery();
            rs.first();

            do {
                BigDecimal lucros = rs.getBigDecimal("sv_lucro");
                lucrosTotais = lucrosTotais.add(lucros);
                dados.add(new Object[]{rs.getDate("sv_data"), rs.getString("cl_nome"), rs.getString("sv_aparelho"), rs.getString("sv_defeito"), rs.getString("sv_servico"),
                    rs.getString("sv_senha"), rs.getBigDecimal("sv_valorpeca"), rs.getBigDecimal("sv_valorservico"), rs.getBigDecimal("sv_lucro"), rs.getInt("sv_status"), rs.getInt("sv_id")});
            } while (rs.next());
        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de lucros\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return lucrosTotais;
    }

    public void buscarTodosServicosDeterminadoCliente(String nome) {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"Data", "Nome", "Aparelho", "Defeito", "Situacao", "Senha", "Valor_Peça", "Lucro", "Status", "ID", "Valor_Serv"};

        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select cl_nome, sv_aparelho, sv_defeito, sv_situacao, sv_senha, sv_data, sv_valorpeca, sv_lucro, sv_status, sv_id, "
                            + "sv_valorservico from tb_servicos INNER JOIN tb_clientes ON tb_clientes.cl_id = tb_servicos.sv_cl_idcliente WHERE tb_clientes.cl_nome LIKE '%" + nome + "%'");
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {

                do {
                    dados.add(new Object[]{rs.getDate("sv_data"), rs.getString("cl_nome"), rs.getString("sv_aparelho"), rs.getString("sv_defeito"), rs.getString("sv_situacao"),
                        rs.getString("sv_senha"), rs.getBigDecimal("sv_valorpeca"), rs.getBigDecimal("sv_lucro"), rs.getString("sv_status"), rs.getInt("sv_id"), rs.getBigDecimal("sv_valorservico")});
                } while (rs.next());
            } else {
                //JOptionPane.showMessageDialog(null, "");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de serviços\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtableOrdensServicos.setModel(modelo);
        //Data
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(0).setPreferredWidth(80);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(0).setResizable(true);
        //Nome
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(1).setPreferredWidth(214);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(1).setResizable(true);
        //Aparelho
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(2).setPreferredWidth(120);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(2).setResizable(true);
        //Defeito
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(3).setPreferredWidth(200);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(3).setResizable(true);
        //Situação1
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(4).setPreferredWidth(454);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(4).setResizable(true);
        //Senha
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setPreferredWidth(75);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setResizable(true);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setMaxWidth(0);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setMinWidth(0);
//        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
//        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);
        //Valor da Peça

//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setPreferredWidth(140);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setResizable(true);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);
        //Lucro
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(7).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(7).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);
        //Status
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(8).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(8).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(8).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(8).setMinWidth(0);
        //ID
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(9).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(9).setMinWidth(0);
        //Valor Servico
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(10).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(10).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(10).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(10).setMinWidth(0);

        j_Principal.jtableOrdensServicos.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtableOrdensServicos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtableOrdensServicos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtableOrdensServicos.setRowHeight(30);
        j_Principal.jtableOrdensServicos.setAutoCreateRowSorter(true);
    }
}
