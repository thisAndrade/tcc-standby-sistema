/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componentes;

import java.awt.Color;
import GUI.j_Principal;
/**
 *
 * @author AngeL
 */
public class statusBotoesMenu {
    
    public void atualizarStatusCadServ(){
        j_Principal.menu1_service.setBackground(new Color(102,102,102));
        j_Principal.menu2_register.setBackground(new Color(51,51,51));
        j_Principal.menu3_doneServices.setBackground(new Color(51,51,51));
        j_Principal.menu4_orcament.setBackground(new Color(51,51,51));
        j_Principal.menu5_extrato.setBackground(new Color(51,51,51));
    }
    
    public void atualizarStatusCadCliente(){
        j_Principal.menu2_register.setBackground(new Color(102,102,102));
        j_Principal.menu1_service.setBackground(new Color(51,51,51));
        j_Principal.menu3_doneServices.setBackground(new Color(51,51,51));
        j_Principal.menu4_orcament.setBackground(new Color(51,51,51));
        j_Principal.menu5_extrato.setBackground(new Color(51,51,51));
    }
    
    public void atualizarStatusServProntos(){
        j_Principal.menu3_doneServices.setBackground(new Color(102,102,102));
        j_Principal.menu1_service.setBackground(new Color(51,51,51));
        j_Principal.menu2_register.setBackground(new Color(51,51,51));
        j_Principal.menu4_orcament.setBackground(new Color(51,51,51));
        j_Principal.menu5_extrato.setBackground(new Color(51,51,51));
    }
    
    public void atualizarStatusOrcamento() {
        j_Principal.menu4_orcament.setBackground(new Color(51,51,51));//102, 102, 102
        j_Principal.menu1_service.setBackground(new Color(51,51,51));
        j_Principal.menu2_register.setBackground(new Color(51,51,51));
        j_Principal.menu3_doneServices.setBackground(new Color(51,51,51));
        j_Principal.menu5_extrato.setBackground(new Color(51,51,51));
    }
    
    public void atualizarStatusExtrato(){
        j_Principal.menu5_extrato.setBackground(new Color(102,102,102));//102, 102, 102
        j_Principal.menu1_service.setBackground(new Color(51,51,51));
        j_Principal.menu2_register.setBackground(new Color(51,51,51));
        j_Principal.menu3_doneServices.setBackground(new Color(51,51,51));
        j_Principal.menu4_orcament.setBackground(new Color(51,51,51));
    }
}
