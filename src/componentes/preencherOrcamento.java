package componentes;

import GUI.j_Principal;
import conexao.db_Conexao;
import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import modeltable.model;

public class preencherOrcamento extends db_Conexao {

    Connection conn;
    ResultSet rs;

    public void preencherOrcamentos(String Modelo){

        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Aparelho", "Modelo", "Peça", "Serviço", "Total"};

        try {
            try {
                conn = getConnection();
            } catch (SQLException | ClassNotFoundException e) {
                JOptionPane.showMessageDialog(null, "Não foi possivel conectar, reinicie o Banco de Dados\n\nERRO: " + e + "", "ERRO", JOptionPane.ERROR_MESSAGE);
            }

            PreparedStatement create = conn.prepareStatement("SELECT * from tb_orcamento where orc_aparelho = ?");
            create.setString(1, Modelo);
            rs = create.executeQuery();

            rs.first();

            if (rs.first() == true) {
                do {
                    BigDecimal peca = rs.getBigDecimal("orc_peca");
                    BigDecimal valor = rs.getBigDecimal("orc_valor");
                    BigDecimal resultado = valor.subtract(peca);
                    dados.add(new Object[]{rs.getInt("orc_id"), rs.getString("orc_aparelho"), rs.getString("orc_modelo"), rs.getBigDecimal("orc_peca"), rs.getBigDecimal("orc_valor"), resultado});
                } while (rs.next());
            } else {
                //JOptionPane.showMessageDialog(null, "Sem dados no banco!", "Lista Vazia", JOptionPane.INFORMATION_MESSAGE);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"ERRO: "+e+"\n\n\nChama o melhor programador da cidade\nque ele resolve.","ERRO", JOptionPane.ERROR_MESSAGE);
        }
        model modelo = new model(dados, colunas);
        j_Principal.jtable_orcamentos.setModel(modelo);

        // ID
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMinWidth(0);
        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
        //Aparelho
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setPreferredWidth(88);
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setResizable(false);

        //Modelo
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setPreferredWidth(150);
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setResizable(false);

        //Peca
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setPreferredWidth(60);
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setResizable(false);

        //Valor Servico
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setPreferredWidth(60);
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setResizable(false);

        //Total
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(5).setPreferredWidth(75);
        j_Principal.jtable_orcamentos.getColumnModel().getColumn(5).setResizable(false);

        //Ordem do table
        j_Principal.jtable_orcamentos.getTableHeader().setReorderingAllowed(false);
        //Tamanho automatico
        j_Principal.jtable_orcamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        //Seleção Singular
        j_Principal.jtable_orcamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtable_orcamentos.setRowHeight(20);
        j_Principal.jtable_orcamentos.setAutoCreateRowSorter(true);
    }

//    public void preencherSamsung() throws SQLException {
//        ArrayList dados = new ArrayList();
//        String[] colunas = new String[]{"ID", "Aparelho", "Modelo", "Peça", "Serviço"};
//
////        try {
//        try {
//            conn = getConnection();
//        } catch (SQLException | ClassNotFoundException e) {
//            JOptionPane.showMessageDialog(null, "ERRO", "Não foi possivel conectar, reinicie o Banco de Dados\n\nERRO: " + e + "", JOptionPane.ERROR_MESSAGE);
//        }
//
//        PreparedStatement create = conn.prepareStatement("SELECT * from tb_orcamento where orc_aparelho = 'SAMSUNG'");
//
//        rs = create.executeQuery();
//
//        rs.first();
//
//        do {
//            dados.add(new Object[]{rs.getInt("orc_id"), rs.getString("orc_aparelho"), rs.getString("orc_modelo"), rs.getBigDecimal("orc_peca"), rs.getBigDecimal("orc_valor")});
//        } while (rs.next());
//
////        } catch (ClassNotFoundException | SQLException e) {
////        }
//        model modelo = new model(dados, colunas);
//        j_Principal.jtable_orcamentos.setModel(modelo);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setPreferredWidth(200);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setPreferredWidth(80);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setPreferredWidth(80);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setResizable(false);
//
//        //Ordem do table
//        j_Principal.jtable_orcamentos.getTableHeader().setReorderingAllowed(false);
//        //Tamanho automatico
//        j_Principal.jtable_orcamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//        //Seleção Singular
//        j_Principal.jtable_orcamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//
//        j_Principal.jtable_orcamentos.setRowHeight(20);
//    }
//
//    public void preencherMotorola() throws SQLException {
//        ArrayList dados = new ArrayList();
//        String[] colunas = new String[]{"ID", "Aparelho", "Modelo", "Peça", "Serviço"};
//        try {
//
//            conn = getConnection();
//
//            PreparedStatement create = conn.prepareStatement("SELECT * From tb_orcamento where orc_aparelho = 'MOTOROLA'");
//
//            rs = create.executeQuery();
//
//            rs.first();
//
//            do {
//                dados.add(new Object[]{rs.getInt("orc_id"), rs.getString("orc_aparelho"), rs.getString("orc_modelo"), rs.getString("orc_peca"), rs.getString("orc_valor")});
//            } while (rs.next());
//        } catch (ClassNotFoundException | SQLException e) {
//        }
//
//        model modelo = new model(dados, colunas);
//        j_Principal.jtable_orcamentos.setModel(modelo);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setPreferredWidth(200);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setPreferredWidth(80);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setPreferredWidth(80);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getTableHeader().setReorderingAllowed(true);
//
//        j_Principal.jtable_orcamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//
//        j_Principal.jtable_orcamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//
//        j_Principal.jtable_orcamentos.setRowHeight(20);
//    }
//
//    public void preencherLG() throws SQLException {
//        ArrayList dados = new ArrayList();
//        String[] colunas = new String[]{"ID", "Aparelho", "Modelo", "Peça", "Serviço"};
//        try {
//
//            conn = getConnection();
//
//            PreparedStatement create = conn.prepareStatement("SELECT * from tb_orcamento where orc_aparelho = 'LG'");
//
//            rs = create.executeQuery();
//
//            rs.first();
//
//            do {
//                dados.add(new Object[]{rs.getInt("orc_id"), rs.getString("orc_aparelho"), rs.getString("orc_modelo"), rs.getString("orc_peca"), rs.getString("orc_valor")});
//            } while (rs.next());
//        } catch (ClassNotFoundException | SQLException e) {
//        }
//
//        model modelo = new model(dados, colunas);
//        j_Principal.jtable_orcamentos.setModel(modelo);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setPreferredWidth(180);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setPreferredWidth(60);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setPreferredWidth(64);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getTableHeader().setReorderingAllowed(true);
//
//        j_Principal.jtable_orcamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//
//        j_Principal.jtable_orcamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//
//        j_Principal.jtable_orcamentos.setRowHeight(20);
//    }
//
//    public void preencherIphone() throws SQLException {
//        ArrayList dados = new ArrayList();
//        String[] colunas = new String[]{"ID", "Aparelho", "Modelo", "Peça", "Serviço"};
//        try {
//
//            conn = getConnection();
//
//            PreparedStatement create = conn.prepareStatement("SELECT * from tb_orcamento where orc_aparelho = 'IPHONE'");
//
//            rs = create.executeQuery();
//
//            rs.first();
//
//            do {
//                dados.add(new Object[]{rs.getInt("orc_id"), rs.getString("orc_aparelho"), rs.getString("orc_modelo"), rs.getString("orc_peca"), rs.getString("orc_valor")});
//            } while (rs.next());
//        } catch (ClassNotFoundException | SQLException e) {
//        }
//
//        model modelo = new model(dados, colunas);
//        j_Principal.jtable_orcamentos.setModel(modelo);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(0).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(1).setMinWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMaxWidth(0);
//        j_Principal.jtable_orcamentos.getTableHeader().getColumnModel().getColumn(1).setMinWidth(0);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setPreferredWidth(180);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(2).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setPreferredWidth(60);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(3).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setPreferredWidth(64);
//        j_Principal.jtable_orcamentos.getColumnModel().getColumn(4).setResizable(false);
//
//        j_Principal.jtable_orcamentos.getTableHeader().setReorderingAllowed(true);
//
//        j_Principal.jtable_orcamentos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//
//        j_Principal.jtable_orcamentos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//
//        j_Principal.jtable_orcamentos.setRowHeight(20);
//    }
}
