/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componentes;

import GUI.j_Principal;
import conexao.db_Conexao;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import modeltable.model;

/**
 *
 * @author AngeL
 */
public class preencher extends db_Conexao {

    Connection conn;
    ResultSet rs;
    Statement stm;

    public void preencherComboboxCliente() throws SQLException, ClassNotFoundException {

        ArrayList<String> dados = new ArrayList<String>();
        conn = getConnection();

        //stm = conn.createStatement();
        PreparedStatement create = conn.prepareStatement("SELECT cl_nome FROM `tb_clientes` ORDER BY cl_nome");
        //rs = stm.executeQuery("select cl_nome from tb_clientes");
        rs = create.executeQuery();
        rs.first();

        if (rs.first() == true) {
            do {
                dados.add(rs.getString("cl_nome"));

            } while (rs.next());
        } else {
            //
        }

        DefaultComboBoxModel comboModel = new DefaultComboBoxModel(dados.toArray());
        j_Principal.jcmbNomeCliente.setModel(comboModel);

    }

    public void preencherTableServicos() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"Data", "Nome", "Aparelho", "Defeito", "Situacao", "Senha", "Valor_Peça", "Lucro", "Status", "ID", "Valor_Serv"};

        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select cl_nome, sv_aparelho, sv_defeito, sv_situacao, sv_senha, sv_data, sv_valorpeca,\n"
                            + "sv_lucro, sv_status, sv_id, sv_valorservico from tb_servicos inner join tb_clientes on tb_clientes.cl_id = tb_servicos.sv_cl_idcliente where sv_status = 1 AND sv_status = 1");
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {

                do {
                    dados.add(new Object[]{rs.getDate("sv_data"), rs.getString("cl_nome"), rs.getString("sv_aparelho"), rs.getString("sv_defeito"), rs.getString("sv_situacao"),
                        rs.getString("sv_senha"), rs.getBigDecimal("sv_valorpeca"), rs.getBigDecimal("sv_lucro"), rs.getString("sv_status"), rs.getInt("sv_id"), rs.getBigDecimal("sv_valorservico")});
                } while (rs.next());
            } else {
                //JOptionPane.showMessageDialog(null, "");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de serviços\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtableOrdensServicos.setModel(modelo);
        //Data
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(0).setPreferredWidth(80);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(0).setResizable(true);
        //Nome
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(1).setPreferredWidth(214);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(1).setResizable(true);
        //Aparelho
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(2).setPreferredWidth(120);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(2).setResizable(true);
        //Defeito
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(3).setPreferredWidth(200);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(3).setResizable(true);
        //Situação1
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(4).setPreferredWidth(454);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(4).setResizable(true);
        //Senha
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setPreferredWidth(75);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setResizable(true);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setMaxWidth(0);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setMinWidth(0);
//        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
//        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);
        //Valor da Peça

//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setPreferredWidth(140);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setResizable(true);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(6).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);
        //Lucro
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(7).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(7).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);
        //Status
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(8).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(8).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(8).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(8).setMinWidth(0);
        //ID
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(9).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(9).setMinWidth(0);
        //Valor Servico
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(10).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(10).setMinWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(10).setMaxWidth(0);
        j_Principal.jtableOrdensServicos.getTableHeader().getColumnModel().getColumn(10).setMinWidth(0);

        j_Principal.jtableOrdensServicos.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtableOrdensServicos.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtableOrdensServicos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtableOrdensServicos.setRowHeight(30);
        j_Principal.jtableOrdensServicos.setAutoCreateRowSorter(true);
    }

    public void preencherRemoveClientes() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Nome", "Telefone", "CPF"};
        try {
            conn = getConnection();
            PreparedStatement create = conn.prepareStatement("select * from tb_clientes");

            rs = create.executeQuery();

            rs.first();

            if (rs.first() == true) {
                do {
                    dados.add(new Object[]{rs.getInt("cl_id"), rs.getString("cl_nome"), rs.getString("cl_telefone"), rs.getString("cl_cpf")});
                } while (rs.next());
            } else {
                //
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Nao foi possivel carregar os dados da tabela", "SEM DADOS", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtableListaClientes.setModel(modelo);
        //id
//        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setPreferredWidth(30);
//        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setResizable(false);

        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setMinWidth(0);
        j_Principal.jtableListaClientes.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtableListaClientes.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

        //Nome
        j_Principal.jtableListaClientes.getColumnModel().getColumn(1).setPreferredWidth(243);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(1).setResizable(false);

        //Telefone
        j_Principal.jtableListaClientes.getColumnModel().getColumn(2).setPreferredWidth(120);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(2).setResizable(false);

        //CPF
        j_Principal.jtableListaClientes.getColumnModel().getColumn(3).setPreferredWidth(130);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(3).setResizable(false);

        j_Principal.jtableListaClientes.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtableListaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtableListaClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtableListaClientes.setRowHeight(30);
        j_Principal.jtableListaClientes.setAutoCreateRowSorter(true);
    }

    public void buscarCliente(String nome) {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Nome", "Telefone", "CPF"};
        try {
            conn = getConnection();
            PreparedStatement create = conn.prepareStatement("select * from tb_clientes where cl_nome like '%" + nome + "%'");
            //create.setString(1, nome);

            rs = create.executeQuery();

            rs.first();

            if (rs.first() == true) {

                do {
                    dados.add(new Object[]{rs.getInt("cl_id"), rs.getString("cl_nome"), rs.getString("cl_telefone"), rs.getString("cl_cpf")});
                } while (rs.next());
            } else {
                //
            }

        } catch (Exception e) {
            //JOptionPane.showMessageDialog(null, "Nao foi possivel carregar os dados da tabela", "SEM DADOS", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtableListaClientes.setModel(modelo);
        //id
//        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setPreferredWidth(30);
//        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setResizable(false);

        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(0).setMinWidth(0);
        j_Principal.jtableListaClientes.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtableListaClientes.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);

        //Nome
        j_Principal.jtableListaClientes.getColumnModel().getColumn(1).setPreferredWidth(243);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(1).setResizable(false);

        //Telefone
        j_Principal.jtableListaClientes.getColumnModel().getColumn(2).setPreferredWidth(120);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(2).setResizable(false);

        //CPF
        j_Principal.jtableListaClientes.getColumnModel().getColumn(3).setPreferredWidth(130);
        j_Principal.jtableListaClientes.getColumnModel().getColumn(3).setResizable(false);

        j_Principal.jtableListaClientes.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtableListaClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtableListaClientes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtableListaClientes.setRowHeight(30);
        j_Principal.jtableListaClientes.setAutoCreateRowSorter(true);
    }

    public void preencherTableGastosLucros() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"Data", "Nome", "Aparelho", "Defeito", "Servico", "Senha", "Peça R$", "Servico R$", "Lucro R$", "Status", "ID"};

        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select cl_nome, sv_aparelho, sv_defeito, sv_servico, sv_senha, sv_data, sv_valorpeca,\n"
                            + "sv_lucro, sv_status, sv_id, sv_valorservico from tb_servicos inner join tb_clientes on tb_clientes.cl_id = tb_servicos.sv_cl_idcliente where sv_status = 0 AND sv_ativo = 1");
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {
                do {
                    dados.add(new Object[]{rs.getDate("sv_data"), rs.getString("cl_nome"), rs.getString("sv_aparelho"), rs.getString("sv_defeito"), rs.getString("sv_servico"),
                        rs.getString("sv_senha"), rs.getBigDecimal("sv_valorpeca"), rs.getBigDecimal("sv_valorservico"), rs.getBigDecimal("sv_lucro"), rs.getInt("sv_status"), rs.getInt("sv_id")});
                } while (rs.next());
            } else {
                //
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de lucros\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtable_GastosLucros.setModel(modelo);
        //Data
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(0).setPreferredWidth(80);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(0).setResizable(true);
        //Nome
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(1).setPreferredWidth(214);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(1).setResizable(true);
        //Aparelho
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(2).setPreferredWidth(120);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(2).setResizable(true);
        //Defeito
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(3).setPreferredWidth(200);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(3).setResizable(true);
        //Situação1//Servico
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(4).setPreferredWidth(300);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(4).setResizable(true);

        //Senha
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setPreferredWidth(140);
//        j_Principal.jtableOrdensServicos.getColumnModel().getColumn(5).setResizable(true);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(5).setMaxWidth(0);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(5).setMinWidth(0);
        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);

        //Valor da Peça
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(6).setPreferredWidth(60);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(6).setResizable(true);
//        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(6).setMaxWidth(0);
//        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(6).setMinWidth(0);
//        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
//        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);

        //Valor Servico
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(7).setPreferredWidth(80);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(7).setResizable(true);
//        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(10).setMaxWidth(0);
//        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(10).setMinWidth(0);
//        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(10).setMaxWidth(0);
//        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(10).setMinWidth(0);

        //Lucro
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(8).setPreferredWidth(70);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(8).setResizable(true);
//        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(7).setMaxWidth(0);
//        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(7).setMinWidth(0);
//        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
//        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);

        //Status
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(9).setMinWidth(0);
        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(9).setMinWidth(0);

        //ID
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(10).setMaxWidth(0);
        j_Principal.jtable_GastosLucros.getColumnModel().getColumn(10).setMinWidth(0);
        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(10).setMaxWidth(0);
        j_Principal.jtable_GastosLucros.getTableHeader().getColumnModel().getColumn(10).setMinWidth(0);

        j_Principal.jtable_GastosLucros.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtable_GastosLucros.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtable_GastosLucros.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtable_GastosLucros.setRowHeight(30);
        j_Principal.jtable_GastosLucros.setAutoCreateRowSorter(true);
    }

    public void preencherTableGastosPessoais() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Data", "Produto", "Valor"};

        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select * from tb_gastos where gst_temporario = 0 AND gst_ativo = 1");
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {
                do {
                    dados.add(new Object[]{rs.getInt("gst_id"), rs.getDate("gst_data"), rs.getString("gst_produto"), rs.getBigDecimal("gst_valor")});
                } while (rs.next());
            } else {
                //
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de gastos pessoais\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtable_GastosPessoais.setModel(modelo);
        //ID
//        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(0).setPreferredWidth(80);
//        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(0).setResizable(true);
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(0).setMinWidth(0);
        j_Principal.jtable_GastosPessoais.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtable_GastosPessoais.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        //Data
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(1).setPreferredWidth(80);
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(1).setResizable(true);
        //Produto
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(2).setPreferredWidth(172);
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(2).setResizable(true);
        //Valor
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(3).setPreferredWidth(70);
        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(3).setResizable(true);

        j_Principal.jtable_GastosPessoais.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtable_GastosPessoais.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtable_GastosPessoais.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtable_GastosPessoais.setRowHeight(30);
        j_Principal.jtable_GastosPessoais.setAutoCreateRowSorter(true);
    }

    public void preencherTableGastosPessoaisTemporarios() {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"ID", "Data", "Produto", "Valor"};

        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select * from tb_gastos where gst_temporario = 1 AND gst_ativo = 1");
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {
                do {
                    dados.add(new Object[]{rs.getInt("gst_id"), rs.getDate("gst_data"), rs.getString("gst_produto"), rs.getBigDecimal("gst_valor")});
                } while (rs.next());
            } else {
                //
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de gastos pessoais\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtable_GastosPessoaisTemporarios.setModel(modelo);
        //ID
//        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(0).setPreferredWidth(80);
//        j_Principal.jtable_GastosPessoais.getColumnModel().getColumn(0).setResizable(true);
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(0).setMinWidth(0);
        j_Principal.jtable_GastosPessoaisTemporarios.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
        j_Principal.jtable_GastosPessoaisTemporarios.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        //Data
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(1).setPreferredWidth(80);
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(1).setResizable(true);
        //Produto
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(2).setPreferredWidth(172);
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(2).setResizable(true);
        //Valor
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(3).setPreferredWidth(70);
        j_Principal.jtable_GastosPessoaisTemporarios.getColumnModel().getColumn(3).setResizable(true);

        j_Principal.jtable_GastosPessoaisTemporarios.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtable_GastosPessoaisTemporarios.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtable_GastosPessoaisTemporarios.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtable_GastosPessoaisTemporarios.setRowHeight(30);
        j_Principal.jtable_GastosPessoaisTemporarios.setAutoCreateRowSorter(true);
    }

    public void preencherTableExtrato(String dataInicial, String dataFinal) {
        ArrayList dados = new ArrayList();
        String[] colunas = new String[]{"Data", "Nome", "Aparelho", "Defeito", "Situacao", "Senha", "Peça R$", "Lucro", "Status", "ID", "Serviço R$"};

        try {
            conn = getConnection();
            PreparedStatement create
                    = conn.prepareStatement("select cl_nome, sv_aparelho, sv_defeito, sv_situacao, sv_senha, sv_data, sv_valorpeca,\n"
                            + "sv_lucro, sv_status, sv_id, sv_valorservico from tb_servicos inner join tb_clientes on tb_clientes.cl_id = tb_servicos.sv_cl_idcliente WHERE sv_data BETWEEN (?) AND (?) AND sv_status = 0");
            create.setString(1, dataInicial);
            create.setString(2, dataFinal);
            rs = create.executeQuery();
            rs.first();

            if (rs.first() == true) {

                do {
                    dados.add(new Object[]{rs.getDate("sv_data"), rs.getString("cl_nome"), rs.getString("sv_aparelho"), rs.getString("sv_defeito"), rs.getString("sv_situacao"),
                        rs.getString("sv_senha"), rs.getBigDecimal("sv_valorpeca"), rs.getBigDecimal("sv_lucro"), rs.getString("sv_status"), rs.getInt("sv_id"), rs.getBigDecimal("sv_valorservico")});
                } while (rs.next());
            } else {
                //JOptionPane.showMessageDialog(null, "");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar preencher a lista de extrato\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        model modelo = new model(dados, colunas);
        j_Principal.jtable_Extrato.setModel(modelo);
        //Data
        j_Principal.jtable_Extrato.getColumnModel().getColumn(0).setPreferredWidth(90);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(0).setResizable(true);
        //Nome
        j_Principal.jtable_Extrato.getColumnModel().getColumn(1).setPreferredWidth(214);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(1).setResizable(true);
        //Aparelho
        j_Principal.jtable_Extrato.getColumnModel().getColumn(2).setPreferredWidth(120);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(2).setResizable(true);
        //Defeito
        j_Principal.jtable_Extrato.getColumnModel().getColumn(3).setPreferredWidth(200);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(3).setResizable(true);
        //Situação1
        j_Principal.jtable_Extrato.getColumnModel().getColumn(4).setPreferredWidth(350);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(4).setResizable(true);
        //Senha
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(5).setPreferredWidth(75);
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(5).setResizable(true);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(5).setMaxWidth(0);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(5).setMinWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(5).setMaxWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(5).setMinWidth(0);
        //Valor da Peça
        j_Principal.jtable_Extrato.getColumnModel().getColumn(6).setPreferredWidth(70);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(6).setResizable(true);
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(6).setMaxWidth(0);
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(6).setMinWidth(0);
//        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(6).setMaxWidth(0);
//        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(6).setMinWidth(0);
        //Lucro
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(7).setPreferredWidth(70);
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(7).setResizable(true);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(7).setMaxWidth(0);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(7).setMinWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(7).setMaxWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(7).setMinWidth(0);
        //Status
        j_Principal.jtable_Extrato.getColumnModel().getColumn(8).setMaxWidth(0);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(8).setMinWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(8).setMaxWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(8).setMinWidth(0);
        //ID
        j_Principal.jtable_Extrato.getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(9).setMinWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(9).setMaxWidth(0);
        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(9).setMinWidth(0);
        //Valor Servico
        j_Principal.jtable_Extrato.getColumnModel().getColumn(10).setPreferredWidth(70);
        j_Principal.jtable_Extrato.getColumnModel().getColumn(10).setResizable(true);
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(10).setMaxWidth(0);
//        j_Principal.jtable_Extrato.getColumnModel().getColumn(10).setMinWidth(0);
//        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(10).setMaxWidth(0);
//        j_Principal.jtable_Extrato.getTableHeader().getColumnModel().getColumn(10).setMinWidth(0);

        j_Principal.jtable_Extrato.getTableHeader().setReorderingAllowed(false);
        j_Principal.jtable_Extrato.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        j_Principal.jtable_Extrato.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        j_Principal.jtable_Extrato.setRowHeight(30);
        j_Principal.jtable_Extrato.setAutoCreateRowSorter(true);
    }
}
