/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package componentes;

import GUI.Loading;
import GUI.j_Principal;
import static java.lang.Thread.sleep;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author adria
 */
public class abrirForm {
       
    public static void main(String[] args) {
        
        Timer timer = new Timer();
        final long SEGUNDOS = (1000*2);
        Loading load = new Loading();
        //j_Principal principal = new j_Principal();
        load.setVisible(true);
        
        try {
            sleep(2000);
        } catch (Exception e) {
        }
        
        TimerTask tarefa = new TimerTask() {
            @Override
            public void run() {
                //vai ser repetido
                load.dispose();
                new j_Principal().setVisible(true);
                timer.cancel();
            }
        };
        
        timer.scheduleAtFixedRate(tarefa, 0, SEGUNDOS);
    }
}
