package GUI;

import componentes.preencher;
import componentes.preencherOrcamento;
import componentes.statusBotoesMenu;
import dados.alterarDados;
import dados.buscarDados;
import dados.deletarDados;
import dados.inserirDados;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import static java.lang.Thread.sleep;
import java.math.BigDecimal;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author adria
 */
public class j_Principal extends javax.swing.JFrame {

    int xx, yy;
    LocalDateTime dataHoje = LocalDateTime.now();
    SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    preencher p = new preencher();
    preencherOrcamento p_orc = new preencherOrcamento();
    alterarDados a = new alterarDados();
    inserirDados i = new inserirDados();
    deletarDados d = new deletarDados();
    buscarDados b = new buscarDados();
    statusBotoesMenu attStatus = new statusBotoesMenu();
    Loading load = new Loading();

    public j_Principal() {
        //jintelitype

//        cheatAbrirForm();
//        this.setVisible(false);
        PegarTeclaPressionada();
        initComponents();
        AutoCompleteDecorator.decorate(jcmbNomeCliente);
        jcmbNomeCliente.setBackground(new Color(0, 0, 0, 0));

        //Setar o icone do projeto
        this.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_400px.png")).getImage());
        jFrame1.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_400px.png")).getImage());
        jfr_AlterarCliente.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_400px.png")).getImage());
        jfr_Orcamento.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_400px.png")).getImage());
        jfr_Orc_AddPecas.setIconImage(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_400px.png")).getImage());

        //Preencher todos os dados do sistema.
        preencherTodasTabelas();

        //ID's de dados que vao ser usados para inserts
        lblSV_ID.setVisible(false);
        lblIdClienteEdit.setVisible(false);

        //Checkbox selecionada padrao e transparencia.
        checkGastos.setBackground(new Color(0, 0, 0, 0));
        checkGastosTemp.setBackground(new Color(0, 0, 0, 0));
        checkGastos.setSelected(true);

        jcmbNomeCliente.requestFocus();
//        txtAparelho.requestFocus();

//        String dt = dateFormat.format(dataHoje);
//        System.out.println("DATA HOJE : "+dt);
        //addSamsung.setBackground(new Color(40, 40, 40));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        pane_form1 = new javax.swing.JPanel();
        btnSAIR_form1 = new javax.swing.JButton();
        lblSV_ID = new javax.swing.JLabel();
        topside = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        txtSenhaEdit = new javax.swing.JTextField();
        lblSenhaEdit = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txtPaneSituacaoEdit = new javax.swing.JTextPane();
        txtDefeitoEdit = new javax.swing.JTextField();
        txtNomeEdit = new javax.swing.JTextField();
        txtAparelhoEdit = new javax.swing.JTextField();
        lblNomeEdit = new javax.swing.JLabel();
        lblAparelhoEdit = new javax.swing.JLabel();
        lblDefeitoEdit = new javax.swing.JLabel();
        lblSituacaoEdit = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnSaveServicoEdit = new javax.swing.JButton();
        btnConcluirServico = new javax.swing.JButton();
        txtServicoEdit = new javax.swing.JTextField();
        txtDataEdit = new javax.swing.JTextField();
        txtLucroEdit = new javax.swing.JTextField();
        txtValorServicoEdit = new javax.swing.JTextField();
        txtValorPecaEdit = new javax.swing.JTextField();
        lblValorPecaEdit = new javax.swing.JLabel();
        lblValorServicoEdit = new javax.swing.JLabel();
        lblLucroEdit = new javax.swing.JLabel();
        lblDataEdit = new javax.swing.JLabel();
        lblServicoEdit = new javax.swing.JLabel();
        jfr_Orcamento = new javax.swing.JFrame();
        bg_orcamento = new javax.swing.JPanel();
        topside_orcamento = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtable_orcamentos = new javax.swing.JTable();
        jpanel_iphone = new javax.swing.JPanel();
        lblIphone = new javax.swing.JLabel();
        jpanel_samsung = new javax.swing.JPanel();
        lblSamsung = new javax.swing.JLabel();
        jpanel_xiaomi = new javax.swing.JPanel();
        lblXiaomi = new javax.swing.JLabel();
        jpanel_LG = new javax.swing.JPanel();
        lblLG = new javax.swing.JLabel();
        jpanel_motorola = new javax.swing.JPanel();
        lblMotorola = new javax.swing.JLabel();
        jpanel_lenovo = new javax.swing.JPanel();
        lblLenovo = new javax.swing.JLabel();
        jpanel_alcatel = new javax.swing.JPanel();
        lblAlcatel = new javax.swing.JLabel();
        jpanel_asus = new javax.swing.JPanel();
        lblAsus = new javax.swing.JLabel();
        btnSAIR_form2 = new javax.swing.JButton();
        jpanel_positivo = new javax.swing.JPanel();
        lblPositivo = new javax.swing.JLabel();
        btn_AddPositivo = new javax.swing.JButton();
        btn_AddSamsung = new javax.swing.JButton();
        btn_AddMotorola = new javax.swing.JButton();
        btn_AddLG = new javax.swing.JButton();
        btn_AddIphone = new javax.swing.JButton();
        btn_AddXiaomi = new javax.swing.JButton();
        btn_AddLenovo = new javax.swing.JButton();
        btn_AddAlcatel = new javax.swing.JButton();
        btn_AddAsus = new javax.swing.JButton();
        jfr_Orc_AddPecas = new javax.swing.JFrame();
        pane_form2 = new javax.swing.JPanel();
        btnSAIR_AddPecas = new javax.swing.JButton();
        lblPecaAddPecas = new javax.swing.JLabel();
        lblModeloAddPecas = new javax.swing.JLabel();
        lblServicoAddPecas = new javax.swing.JLabel();
        txtModeloAddPEcas = new javax.swing.JTextField();
        txtServicoAddPecas = new javax.swing.JTextField();
        txtPecaAddPecas = new javax.swing.JTextField();
        btnAdicionarPecaNova = new javax.swing.JButton();
        txtMarcaAddPecas = new javax.swing.JTextField();
        jfr_AlterarCliente = new javax.swing.JFrame();
        pane_form3 = new javax.swing.JPanel();
        btnSAIR_AddPecas1 = new javax.swing.JButton();
        lblPecaAddPecas1 = new javax.swing.JLabel();
        lblModeloAddPecas1 = new javax.swing.JLabel();
        lblServicoAddPecas1 = new javax.swing.JLabel();
        txtNomeClienteEdit = new javax.swing.JTextField();
        txtTelefoneClienteEdit = new javax.swing.JTextField();
        btnEditarCliente = new javax.swing.JButton();
        lblIdClienteEdit = new javax.swing.JLabel();
        txtCpfClienteEdit = new javax.swing.JFormattedTextField();
        pane_TopSide2 = new javax.swing.JPanel();
        pane_TopSide1 = new javax.swing.JPanel();
        CardLayout = new javax.swing.JPanel();
        pane_Ordens = new javax.swing.JPanel();
        btnSAIR_ordens = new javax.swing.JButton();
        pane_Add_Entrada = new javax.swing.JPanel();
        lblAddEntrada = new javax.swing.JLabel();
        pane_Remove_Entrada = new javax.swing.JPanel();
        lblRemoveEntrada = new javax.swing.JLabel();
        pane_Servicos = new javax.swing.JPanel();
        lblOrdensTOPMENU = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtableOrdensServicos = new javax.swing.JTable();
        txtAparelho = new javax.swing.JTextField();
        txtDefeito = new javax.swing.JTextField();
        txtSituacao = new javax.swing.JTextField();
        txtSenha = new javax.swing.JTextField();
        lblVoltar = new javax.swing.JLabel();
        jcmbNomeCliente = new javax.swing.JComboBox();
        atras_situacao = new javax.swing.JPanel();
        lblSituacao = new javax.swing.JLabel();
        atras_defeito = new javax.swing.JPanel();
        lblDefeito = new javax.swing.JLabel();
        atras_aparelho = new javax.swing.JPanel();
        lblAparelho = new javax.swing.JLabel();
        atras_valor = new javax.swing.JPanel();
        lblValor = new javax.swing.JLabel();
        atras_clienteCOMBO = new javax.swing.JTextField();
        atras_cliente = new javax.swing.JPanel();
        lblCliente = new javax.swing.JLabel();
        txtBuscarServicosCliente = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        pane_Cad_Cliente = new javax.swing.JPanel();
        btnSAIR_cliente = new javax.swing.JButton();
        lblNome = new javax.swing.JLabel();
        lblCPF = new javax.swing.JLabel();
        lblTelefone = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtTelefone = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnRegistrarCliente = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtableListaClientes = new javax.swing.JTable();
        txtProcurarCliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCPF = new javax.swing.JFormattedTextField();
        pane_GastosLucros = new javax.swing.JPanel();
        btnSAIR_removeCl = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        jtable_GastosLucros = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        jtable_GastosPessoais = new javax.swing.JTable();
        jpaneGastos = new javax.swing.JPanel();
        lblGastosPessoaisText = new javax.swing.JLabel();
        lblGastosPessoais = new javax.swing.JLabel();
        lblLucroServicosText = new javax.swing.JLabel();
        lblLucrosServicosDone = new javax.swing.JLabel();
        lblLucroTotalText = new javax.swing.JLabel();
        lblLucroTotal = new javax.swing.JLabel();
        separador1 = new javax.swing.JSeparator();
        separador2 = new javax.swing.JSeparator();
        separador3 = new javax.swing.JSeparator();
        separador4 = new javax.swing.JSeparator();
        lblProdutoGastosPessoais = new javax.swing.JLabel();
        txtProdutoGastosPessoais = new javax.swing.JTextField();
        lblValorGastosPessoais = new javax.swing.JLabel();
        txtValorGastosPessoais = new javax.swing.JTextField();
        btnAddProdutoGastosPessoais = new javax.swing.JButton();
        pane_Servicos3 = new javax.swing.JPanel();
        lblOrdensTOPMENU3 = new javax.swing.JLabel();
        pane_Servicos4 = new javax.swing.JPanel();
        lblOrdensTOPMENU5 = new javax.swing.JLabel();
        pane_Servicos1 = new javax.swing.JPanel();
        lblOrdensTOPMENU1 = new javax.swing.JLabel();
        pane_Servicos2 = new javax.swing.JPanel();
        lblOrdensTOPMENU2 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        jtable_GastosPessoaisTemporarios = new javax.swing.JTable();
        checkGastos = new javax.swing.JCheckBox();
        checkGastosTemp = new javax.swing.JCheckBox();
        pane_Extrato = new javax.swing.JPanel();
        btnSAIR_removeCl1 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jtable_Extrato = new javax.swing.JTable();
        jdateInicial = new com.toedter.calendar.JDateChooser();
        jdateFinal = new com.toedter.calendar.JDateChooser();
        btnVerExtrato = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jpanelLucroExtrato = new javax.swing.JPanel();
        lblLucroExtrato = new javax.swing.JLabel();
        pane_Menu = new javax.swing.JPanel();
        menu1_service = new javax.swing.JPanel();
        lblOrdensServico = new javax.swing.JLabel();
        menu2_register = new javax.swing.JPanel();
        lblCad_Cliente = new javax.swing.JLabel();
        menu3_doneServices = new javax.swing.JPanel();
        lblServicos_Done = new javax.swing.JLabel();
        menu4_orcament = new javax.swing.JPanel();
        lblOrcamento = new javax.swing.JLabel();
        menu5_extrato = new javax.swing.JPanel();
        lblExtrato = new javax.swing.JLabel();
        divisoria = new javax.swing.JPanel();
        lblLogoStandby = new javax.swing.JLabel();

        jFrame1.setTitle("Editar Serviço");
        jFrame1.setBackground(new java.awt.Color(102, 102, 102));
        jFrame1.setMaximumSize(new java.awt.Dimension(517, 660));
        jFrame1.setMinimumSize(new java.awt.Dimension(517, 660));
        jFrame1.setUndecorated(true);
        jFrame1.setPreferredSize(new java.awt.Dimension(517, 660));
        jFrame1.setResizable(false);
        jFrame1.setSize(new java.awt.Dimension(517, 660));
        jFrame1.getContentPane().setLayout(null);

        pane_form1.setBackground(new java.awt.Color(0, 0, 0));
        pane_form1.setMaximumSize(new java.awt.Dimension(519, 639));
        pane_form1.setMinimumSize(new java.awt.Dimension(519, 639));
        pane_form1.setPreferredSize(new java.awt.Dimension(519, 639));
        pane_form1.setLayout(null);

        btnSAIR_form1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_form1.setBorder(null);
        btnSAIR_form1.setContentAreaFilled(false);
        btnSAIR_form1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_form1ActionPerformed(evt);
            }
        });
        pane_form1.add(btnSAIR_form1);
        btnSAIR_form1.setBounds(490, 0, 30, 30);

        lblSV_ID.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblSV_ID.setText("ID");
        pane_form1.add(lblSV_ID);
        lblSV_ID.setBounds(0, 160, 40, 40);

        topside.setBackground(new java.awt.Color(0, 0, 0));
        topside.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                topsideMouseDragged(evt);
            }
        });
        topside.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                topsideMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                topsideMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout topsideLayout = new javax.swing.GroupLayout(topside);
        topside.setLayout(topsideLayout);
        topsideLayout.setHorizontalGroup(
            topsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 490, Short.MAX_VALUE)
        );
        topsideLayout.setVerticalGroup(
            topsideLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        pane_form1.add(topside);
        topside.setBounds(0, 0, 490, 30);

        jPanel3.setBackground(new java.awt.Color(70, 70, 70));

        txtSenhaEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtSenhaEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtSenhaEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSenhaEdit.setBorder(null);
        txtSenhaEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSenhaEditKeyPressed(evt);
            }
        });

        lblSenhaEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblSenhaEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblSenhaEdit.setText("Senha :");

        txtPaneSituacaoEdit.setFont(new java.awt.Font("Arial", 1, 13)); // NOI18N
        txtPaneSituacaoEdit.setNextFocusableComponent(txtSenhaEdit);
        txtPaneSituacaoEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPaneSituacaoEditKeyPressed(evt);
            }
        });
        jScrollPane8.setViewportView(txtPaneSituacaoEdit);

        txtDefeitoEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtDefeitoEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtDefeitoEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDefeitoEdit.setBorder(null);
        txtDefeitoEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDefeitoEditKeyPressed(evt);
            }
        });

        txtNomeEdit.setFont(new java.awt.Font("Century Gothic", 1, 16)); // NOI18N
        txtNomeEdit.setForeground(new java.awt.Color(150, 150, 150));
        txtNomeEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNomeEdit.setBorder(null);

        txtAparelhoEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtAparelhoEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtAparelhoEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAparelhoEdit.setBorder(null);
        txtAparelhoEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAparelhoEditKeyPressed(evt);
            }
        });

        lblNomeEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblNomeEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblNomeEdit.setText("Nome :");

        lblAparelhoEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblAparelhoEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblAparelhoEdit.setText("Aparelho :");

        lblDefeitoEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblDefeitoEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblDefeitoEdit.setText("Defeito :");

        lblSituacaoEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblSituacaoEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblSituacaoEdit.setText("Situacao :");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(lblNomeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtNomeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblAparelhoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtAparelhoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblDefeitoEdit)
                        .addGap(10, 10, 10)
                        .addComponent(txtDefeitoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(lblSituacaoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(lblSenhaEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(txtSenhaEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(89, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNomeEdit)
                    .addComponent(txtNomeEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblAparelhoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAparelhoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDefeitoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDefeitoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(lblSituacaoEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSenhaEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSenhaEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pane_form1.add(jPanel3);
        jPanel3.setBounds(40, 30, 440, 330);

        jPanel4.setBackground(new java.awt.Color(50, 50, 50));
        jPanel4.setLayout(null);

        btnSaveServicoEdit.setBackground(new java.awt.Color(40, 40, 40));
        btnSaveServicoEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_save_all_40px.png"))); // NOI18N
        btnSaveServicoEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveServicoEditActionPerformed(evt);
            }
        });
        jPanel4.add(btnSaveServicoEdit);
        btnSaveServicoEdit.setBounds(110, 210, 40, 40);

        btnConcluirServico.setBackground(new java.awt.Color(40, 40, 40));
        btnConcluirServico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_tick_box_50px.png"))); // NOI18N
        btnConcluirServico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConcluirServicoActionPerformed(evt);
            }
        });
        jPanel4.add(btnConcluirServico);
        btnConcluirServico.setBounds(310, 210, 40, 40);

        txtServicoEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtServicoEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtServicoEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtServicoEdit.setNextFocusableComponent(txtAparelhoEdit);
        txtServicoEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtServicoEditKeyPressed(evt);
            }
        });
        jPanel4.add(txtServicoEdit);
        txtServicoEdit.setBounds(110, 170, 240, 30);

        txtDataEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtDataEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtDataEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDataEdit.setBorder(null);
        txtDataEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDataEditKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDataEditKeyTyped(evt);
            }
        });
        jPanel4.add(txtDataEdit);
        txtDataEdit.setBounds(110, 130, 240, 30);

        txtLucroEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtLucroEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtLucroEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtLucroEdit.setBorder(null);
        txtLucroEdit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtLucroEditFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtLucroEditFocusLost(evt);
            }
        });
        jPanel4.add(txtLucroEdit);
        txtLucroEdit.setBounds(110, 90, 240, 30);

        txtValorServicoEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtValorServicoEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtValorServicoEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValorServicoEdit.setBorder(null);
        txtValorServicoEdit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtValorServicoEditFocusGained(evt);
            }
        });
        txtValorServicoEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtValorServicoEditKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtValorServicoEditKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorServicoEditKeyTyped(evt);
            }
        });
        jPanel4.add(txtValorServicoEdit);
        txtValorServicoEdit.setBounds(110, 50, 240, 30);

        txtValorPecaEdit.setFont(new java.awt.Font("Arial", 1, 16)); // NOI18N
        txtValorPecaEdit.setForeground(new java.awt.Color(51, 51, 51));
        txtValorPecaEdit.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValorPecaEdit.setBorder(null);
        txtValorPecaEdit.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtValorPecaEditFocusGained(evt);
            }
        });
        txtValorPecaEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtValorPecaEditKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorPecaEditKeyTyped(evt);
            }
        });
        jPanel4.add(txtValorPecaEdit);
        txtValorPecaEdit.setBounds(110, 10, 240, 30);

        lblValorPecaEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblValorPecaEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblValorPecaEdit.setText("Peça R$ :");
        jPanel4.add(lblValorPecaEdit);
        lblValorPecaEdit.setBounds(20, 10, 90, 25);

        lblValorServicoEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblValorServicoEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblValorServicoEdit.setText("Serviço R$ :");
        jPanel4.add(lblValorServicoEdit);
        lblValorServicoEdit.setBounds(0, 50, 100, 25);

        lblLucroEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblLucroEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblLucroEdit.setText("Lucro :");
        jPanel4.add(lblLucroEdit);
        lblLucroEdit.setBounds(40, 90, 60, 25);

        lblDataEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblDataEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblDataEdit.setText("Data :");
        jPanel4.add(lblDataEdit);
        lblDataEdit.setBounds(50, 130, 50, 20);

        lblServicoEdit.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblServicoEdit.setForeground(new java.awt.Color(209, 209, 209));
        lblServicoEdit.setText("Servico :");
        jPanel4.add(lblServicoEdit);
        lblServicoEdit.setBounds(30, 170, 70, 30);

        pane_form1.add(jPanel4);
        jPanel4.setBounds(40, 370, 440, 260);

        jFrame1.getContentPane().add(pane_form1);
        pane_form1.setBounds(0, 0, 520, 660);

        jfr_Orcamento.setTitle("Peças e Modelos");
        jfr_Orcamento.setMinimumSize(new java.awt.Dimension(799, 489));
        jfr_Orcamento.setUndecorated(true);
        jfr_Orcamento.setPreferredSize(new java.awt.Dimension(799, 489));
        jfr_Orcamento.setSize(new java.awt.Dimension(799, 489));
        jfr_Orcamento.getContentPane().setLayout(null);

        bg_orcamento.setBackground(new java.awt.Color(60, 60, 60));
        bg_orcamento.setMaximumSize(new java.awt.Dimension(798, 489));
        bg_orcamento.setMinimumSize(new java.awt.Dimension(798, 489));
        bg_orcamento.setPreferredSize(new java.awt.Dimension(798, 489));
        bg_orcamento.setLayout(null);

        topside_orcamento.setBackground(new java.awt.Color(60, 60, 60));
        topside_orcamento.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                topside_orcamentoMouseDragged(evt);
            }
        });
        topside_orcamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                topside_orcamentoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                topside_orcamentoMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout topside_orcamentoLayout = new javax.swing.GroupLayout(topside_orcamento);
        topside_orcamento.setLayout(topside_orcamentoLayout);
        topside_orcamentoLayout.setHorizontalGroup(
            topside_orcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 770, Short.MAX_VALUE)
        );
        topside_orcamentoLayout.setVerticalGroup(
            topside_orcamentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        bg_orcamento.add(topside_orcamento);
        topside_orcamento.setBounds(0, 0, 770, 30);

        jtable_orcamentos.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N
        jtable_orcamentos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        jtable_orcamentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtable_orcamentosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jtable_orcamentos);

        bg_orcamento.add(jScrollPane3);
        jScrollPane3.setBounds(330, 30, 440, 440);

        jpanel_iphone.setBackground(new java.awt.Color(40, 40, 40));

        lblIphone.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblIphone.setForeground(new java.awt.Color(172, 224, 234));
        lblIphone.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblIphone.setText("iPhone");
        lblIphone.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblIphoneMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblIphoneMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblIphoneMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_iphoneLayout = new javax.swing.GroupLayout(jpanel_iphone);
        jpanel_iphone.setLayout(jpanel_iphoneLayout);
        jpanel_iphoneLayout.setHorizontalGroup(
            jpanel_iphoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanel_iphoneLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblIphone, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_iphoneLayout.setVerticalGroup(
            jpanel_iphoneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblIphone, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_iphone);
        jpanel_iphone.setBounds(10, 180, 260, 40);

        jpanel_samsung.setBackground(new java.awt.Color(40, 40, 40));

        lblSamsung.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblSamsung.setForeground(new java.awt.Color(51, 51, 255));
        lblSamsung.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSamsung.setText("Samsung");
        lblSamsung.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblSamsungMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblSamsungMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblSamsungMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_samsungLayout = new javax.swing.GroupLayout(jpanel_samsung);
        jpanel_samsung.setLayout(jpanel_samsungLayout);
        jpanel_samsungLayout.setHorizontalGroup(
            jpanel_samsungLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanel_samsungLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblSamsung, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_samsungLayout.setVerticalGroup(
            jpanel_samsungLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblSamsung, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_samsung);
        jpanel_samsung.setBounds(10, 30, 260, 40);

        jpanel_xiaomi.setBackground(new java.awt.Color(40, 40, 40));

        lblXiaomi.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblXiaomi.setForeground(new java.awt.Color(0, 204, 204));
        lblXiaomi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblXiaomi.setText("Xiaomi");
        lblXiaomi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblXiaomiMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblXiaomiMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblXiaomiMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_xiaomiLayout = new javax.swing.GroupLayout(jpanel_xiaomi);
        jpanel_xiaomi.setLayout(jpanel_xiaomiLayout);
        jpanel_xiaomiLayout.setHorizontalGroup(
            jpanel_xiaomiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanel_xiaomiLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblXiaomi, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_xiaomiLayout.setVerticalGroup(
            jpanel_xiaomiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblXiaomi, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_xiaomi);
        jpanel_xiaomi.setBounds(10, 230, 260, 40);

        jpanel_LG.setBackground(new java.awt.Color(40, 40, 40));

        lblLG.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblLG.setForeground(new java.awt.Color(255, 0, 102));
        lblLG.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLG.setText("LG");
        lblLG.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblLGMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblLGMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblLGMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_LGLayout = new javax.swing.GroupLayout(jpanel_LG);
        jpanel_LG.setLayout(jpanel_LGLayout);
        jpanel_LGLayout.setHorizontalGroup(
            jpanel_LGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanel_LGLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLG, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_LGLayout.setVerticalGroup(
            jpanel_LGLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblLG, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_LG);
        jpanel_LG.setBounds(10, 130, 260, 40);

        jpanel_motorola.setBackground(new java.awt.Color(40, 40, 40));

        lblMotorola.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblMotorola.setForeground(new java.awt.Color(153, 0, 153));
        lblMotorola.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblMotorola.setText("Motorola");
        lblMotorola.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblMotorolaMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblMotorolaMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblMotorolaMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_motorolaLayout = new javax.swing.GroupLayout(jpanel_motorola);
        jpanel_motorola.setLayout(jpanel_motorolaLayout);
        jpanel_motorolaLayout.setHorizontalGroup(
            jpanel_motorolaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanel_motorolaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMotorola, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_motorolaLayout.setVerticalGroup(
            jpanel_motorolaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblMotorola, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_motorola);
        jpanel_motorola.setBounds(10, 80, 260, 40);

        jpanel_lenovo.setBackground(new java.awt.Color(40, 40, 40));

        lblLenovo.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblLenovo.setForeground(new java.awt.Color(255, 153, 204));
        lblLenovo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLenovo.setText("Lenovo");
        lblLenovo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblLenovoMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblLenovoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblLenovoMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_lenovoLayout = new javax.swing.GroupLayout(jpanel_lenovo);
        jpanel_lenovo.setLayout(jpanel_lenovoLayout);
        jpanel_lenovoLayout.setHorizontalGroup(
            jpanel_lenovoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanel_lenovoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblLenovo, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_lenovoLayout.setVerticalGroup(
            jpanel_lenovoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblLenovo, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_lenovo);
        jpanel_lenovo.setBounds(10, 280, 260, 40);

        jpanel_alcatel.setBackground(new java.awt.Color(40, 40, 40));

        lblAlcatel.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblAlcatel.setForeground(new java.awt.Color(0, 204, 51));
        lblAlcatel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAlcatel.setText("Alcatel");
        lblAlcatel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAlcatelMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblAlcatelMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblAlcatelMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_alcatelLayout = new javax.swing.GroupLayout(jpanel_alcatel);
        jpanel_alcatel.setLayout(jpanel_alcatelLayout);
        jpanel_alcatelLayout.setHorizontalGroup(
            jpanel_alcatelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanel_alcatelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAlcatel, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_alcatelLayout.setVerticalGroup(
            jpanel_alcatelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblAlcatel, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_alcatel);
        jpanel_alcatel.setBounds(10, 330, 260, 40);

        jpanel_asus.setBackground(new java.awt.Color(40, 40, 40));

        lblAsus.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblAsus.setForeground(new java.awt.Color(255, 153, 51));
        lblAsus.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAsus.setText("Asus");
        lblAsus.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAsusMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblAsusMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblAsusMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_asusLayout = new javax.swing.GroupLayout(jpanel_asus);
        jpanel_asus.setLayout(jpanel_asusLayout);
        jpanel_asusLayout.setHorizontalGroup(
            jpanel_asusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanel_asusLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblAsus, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_asusLayout.setVerticalGroup(
            jpanel_asusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblAsus, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_asus);
        jpanel_asus.setBounds(10, 380, 260, 40);

        btnSAIR_form2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_form2.setBorder(null);
        btnSAIR_form2.setContentAreaFilled(false);
        btnSAIR_form2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_form2ActionPerformed(evt);
            }
        });
        bg_orcamento.add(btnSAIR_form2);
        btnSAIR_form2.setBounds(770, 0, 30, 30);

        jpanel_positivo.setBackground(new java.awt.Color(40, 40, 40));

        lblPositivo.setFont(new java.awt.Font("Century Gothic", 0, 24)); // NOI18N
        lblPositivo.setForeground(new java.awt.Color(204, 51, 255));
        lblPositivo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblPositivo.setText("Positivo");
        lblPositivo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblPositivoMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblPositivoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblPositivoMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jpanel_positivoLayout = new javax.swing.GroupLayout(jpanel_positivo);
        jpanel_positivo.setLayout(jpanel_positivoLayout);
        jpanel_positivoLayout.setHorizontalGroup(
            jpanel_positivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanel_positivoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblPositivo, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                .addContainerGap())
        );
        jpanel_positivoLayout.setVerticalGroup(
            jpanel_positivoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblPositivo, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        bg_orcamento.add(jpanel_positivo);
        jpanel_positivo.setBounds(10, 430, 260, 40);

        btn_AddPositivo.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddPositivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddPositivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddPositivoActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddPositivo);
        btn_AddPositivo.setBounds(280, 430, 40, 40);

        btn_AddSamsung.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddSamsung.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddSamsung.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddSamsungActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddSamsung);
        btn_AddSamsung.setBounds(280, 30, 40, 40);

        btn_AddMotorola.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddMotorola.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddMotorola.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddMotorolaActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddMotorola);
        btn_AddMotorola.setBounds(280, 80, 40, 40);

        btn_AddLG.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddLG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddLG.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddLGActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddLG);
        btn_AddLG.setBounds(280, 130, 40, 40);

        btn_AddIphone.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddIphone.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddIphone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddIphoneActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddIphone);
        btn_AddIphone.setBounds(280, 180, 40, 40);

        btn_AddXiaomi.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddXiaomi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddXiaomi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddXiaomiActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddXiaomi);
        btn_AddXiaomi.setBounds(280, 230, 40, 40);

        btn_AddLenovo.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddLenovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddLenovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddLenovoActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddLenovo);
        btn_AddLenovo.setBounds(280, 280, 40, 40);

        btn_AddAlcatel.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddAlcatel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddAlcatel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddAlcatelActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddAlcatel);
        btn_AddAlcatel.setBounds(280, 330, 40, 40);

        btn_AddAsus.setBackground(new java.awt.Color(40, 40, 40));
        btn_AddAsus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_40px_1.png"))); // NOI18N
        btn_AddAsus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddAsusActionPerformed(evt);
            }
        });
        bg_orcamento.add(btn_AddAsus);
        btn_AddAsus.setBounds(280, 380, 40, 40);

        jfr_Orcamento.getContentPane().add(bg_orcamento);
        bg_orcamento.setBounds(0, 0, 800, 490);

        jfr_Orc_AddPecas.setTitle("Adicionar nova Peça");
        jfr_Orc_AddPecas.setMinimumSize(new java.awt.Dimension(411, 296));
        jfr_Orc_AddPecas.setUndecorated(true);
        jfr_Orc_AddPecas.setResizable(false);
        jfr_Orc_AddPecas.setSize(new java.awt.Dimension(411, 296));

        pane_form2.setBackground(new java.awt.Color(60, 60, 60));
        pane_form2.setMaximumSize(new java.awt.Dimension(630, 639));
        pane_form2.setMinimumSize(new java.awt.Dimension(630, 639));
        pane_form2.setLayout(null);

        btnSAIR_AddPecas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_AddPecas.setBorder(null);
        btnSAIR_AddPecas.setContentAreaFilled(false);
        btnSAIR_AddPecas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_AddPecasActionPerformed(evt);
            }
        });
        pane_form2.add(btnSAIR_AddPecas);
        btnSAIR_AddPecas.setBounds(380, 0, 30, 30);

        lblPecaAddPecas.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblPecaAddPecas.setForeground(new java.awt.Color(209, 209, 209));
        lblPecaAddPecas.setText("Peca(R$): ");
        pane_form2.add(lblPecaAddPecas);
        lblPecaAddPecas.setBounds(60, 180, 90, 24);

        lblModeloAddPecas.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblModeloAddPecas.setForeground(new java.awt.Color(209, 209, 209));
        lblModeloAddPecas.setText("Modelo: ");
        pane_form2.add(lblModeloAddPecas);
        lblModeloAddPecas.setBounds(76, 80, 69, 24);

        lblServicoAddPecas.setFont(new java.awt.Font("Century Gothic", 0, 18)); // NOI18N
        lblServicoAddPecas.setForeground(new java.awt.Color(209, 209, 209));
        lblServicoAddPecas.setText("Servico(R$): ");
        pane_form2.add(lblServicoAddPecas);
        lblServicoAddPecas.setBounds(40, 130, 110, 24);

        txtModeloAddPEcas.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        txtModeloAddPEcas.setForeground(new java.awt.Color(150, 150, 150));
        txtModeloAddPEcas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtModeloAddPEcas.setBorder(null);
        txtModeloAddPEcas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtModeloAddPEcasKeyPressed(evt);
            }
        });
        pane_form2.add(txtModeloAddPEcas);
        txtModeloAddPEcas.setBounds(150, 80, 210, 30);

        txtServicoAddPecas.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        txtServicoAddPecas.setForeground(new java.awt.Color(150, 150, 150));
        txtServicoAddPecas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtServicoAddPecas.setBorder(null);
        txtServicoAddPecas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtServicoAddPecasKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtServicoAddPecasKeyTyped(evt);
            }
        });
        pane_form2.add(txtServicoAddPecas);
        txtServicoAddPecas.setBounds(150, 130, 210, 30);

        txtPecaAddPecas.setFont(new java.awt.Font("Century Gothic", 0, 16)); // NOI18N
        txtPecaAddPecas.setForeground(new java.awt.Color(150, 150, 150));
        txtPecaAddPecas.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtPecaAddPecas.setBorder(null);
        txtPecaAddPecas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPecaAddPecasKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPecaAddPecasKeyTyped(evt);
            }
        });
        pane_form2.add(txtPecaAddPecas);
        txtPecaAddPecas.setBounds(150, 180, 210, 30);

        btnAdicionarPecaNova.setBackground(new java.awt.Color(40, 40, 40));
        btnAdicionarPecaNova.setText("Adicionar");
        btnAdicionarPecaNova.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarPecaNovaActionPerformed(evt);
            }
        });
        pane_form2.add(btnAdicionarPecaNova);
        btnAdicionarPecaNova.setBounds(200, 230, 100, 40);

        txtMarcaAddPecas.setEditable(false);
        txtMarcaAddPecas.setText("Marca do Cel");
        pane_form2.add(txtMarcaAddPecas);
        txtMarcaAddPecas.setBounds(10, 10, 80, 20);
        txtMarcaAddPecas.setVisible(false);

        javax.swing.GroupLayout jfr_Orc_AddPecasLayout = new javax.swing.GroupLayout(jfr_Orc_AddPecas.getContentPane());
        jfr_Orc_AddPecas.getContentPane().setLayout(jfr_Orc_AddPecasLayout);
        jfr_Orc_AddPecasLayout.setHorizontalGroup(
            jfr_Orc_AddPecasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 411, Short.MAX_VALUE)
            .addGroup(jfr_Orc_AddPecasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jfr_Orc_AddPecasLayout.createSequentialGroup()
                    .addComponent(pane_form2, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jfr_Orc_AddPecasLayout.setVerticalGroup(
            jfr_Orc_AddPecasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
            .addGroup(jfr_Orc_AddPecasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jfr_Orc_AddPecasLayout.createSequentialGroup()
                    .addComponent(pane_form2, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jfr_AlterarCliente.setMinimumSize(new java.awt.Dimension(411, 296));
        jfr_AlterarCliente.setUndecorated(true);
        jfr_AlterarCliente.setSize(new java.awt.Dimension(411, 296));

        pane_form3.setBackground(new java.awt.Color(60, 60, 60));
        pane_form3.setMaximumSize(new java.awt.Dimension(411, 300));
        pane_form3.setMinimumSize(new java.awt.Dimension(411, 300));
        pane_form3.setPreferredSize(new java.awt.Dimension(411, 300));
        pane_form3.setLayout(null);

        btnSAIR_AddPecas1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_AddPecas1.setBorder(null);
        btnSAIR_AddPecas1.setContentAreaFilled(false);
        btnSAIR_AddPecas1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSAIR_AddPecas1MouseClicked(evt);
            }
        });
        btnSAIR_AddPecas1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_AddPecas1ActionPerformed(evt);
            }
        });
        pane_form3.add(btnSAIR_AddPecas1);
        btnSAIR_AddPecas1.setBounds(380, 0, 30, 30);

        lblPecaAddPecas1.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblPecaAddPecas1.setForeground(new java.awt.Color(209, 209, 209));
        lblPecaAddPecas1.setText("Telefone:");
        pane_form3.add(lblPecaAddPecas1);
        lblPecaAddPecas1.setBounds(10, 180, 80, 25);

        lblModeloAddPecas1.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblModeloAddPecas1.setForeground(new java.awt.Color(209, 209, 209));
        lblModeloAddPecas1.setText("Nome: ");
        pane_form3.add(lblModeloAddPecas1);
        lblModeloAddPecas1.setBounds(30, 80, 70, 25);

        lblServicoAddPecas1.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblServicoAddPecas1.setForeground(new java.awt.Color(209, 209, 209));
        lblServicoAddPecas1.setText("CPF:");
        pane_form3.add(lblServicoAddPecas1);
        lblServicoAddPecas1.setBounds(50, 130, 50, 24);

        txtNomeClienteEdit.setFont(new java.awt.Font("Malgun Gothic Semilight", 0, 18)); // NOI18N
        txtNomeClienteEdit.setNextFocusableComponent(txtCpfClienteEdit);
        txtNomeClienteEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNomeClienteEditKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNomeClienteEditKeyTyped(evt);
            }
        });
        pane_form3.add(txtNomeClienteEdit);
        txtNomeClienteEdit.setBounds(90, 80, 280, 30);

        txtTelefoneClienteEdit.setFont(new java.awt.Font("Malgun Gothic Semilight", 0, 18)); // NOI18N
        txtTelefoneClienteEdit.setNextFocusableComponent(txtNomeClienteEdit);
        txtTelefoneClienteEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTelefoneClienteEditKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefoneClienteEditKeyTyped(evt);
            }
        });
        pane_form3.add(txtTelefoneClienteEdit);
        txtTelefoneClienteEdit.setBounds(90, 180, 280, 30);

        btnEditarCliente.setBackground(new java.awt.Color(60, 60, 60));
        btnEditarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_save_all_40px.png"))); // NOI18N
        btnEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarClienteActionPerformed(evt);
            }
        });
        pane_form3.add(btnEditarCliente);
        btnEditarCliente.setBounds(200, 220, 60, 60);

        lblIdClienteEdit.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblIdClienteEdit.setText("ID");
        pane_form3.add(lblIdClienteEdit);
        lblIdClienteEdit.setBounds(330, 230, 40, 40);

        try {
            txtCpfClienteEdit.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCpfClienteEdit.setFont(new java.awt.Font("Malgun Gothic Semilight", 0, 18)); // NOI18N
        txtCpfClienteEdit.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCpfClienteEditKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCpfClienteEditKeyTyped(evt);
            }
        });
        pane_form3.add(txtCpfClienteEdit);
        txtCpfClienteEdit.setBounds(90, 130, 280, 30);

        javax.swing.GroupLayout jfr_AlterarClienteLayout = new javax.swing.GroupLayout(jfr_AlterarCliente.getContentPane());
        jfr_AlterarCliente.getContentPane().setLayout(jfr_AlterarClienteLayout);
        jfr_AlterarClienteLayout.setHorizontalGroup(
            jfr_AlterarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 411, Short.MAX_VALUE)
            .addGroup(jfr_AlterarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jfr_AlterarClienteLayout.createSequentialGroup()
                    .addComponent(pane_form3, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jfr_AlterarClienteLayout.setVerticalGroup(
            jfr_AlterarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
            .addGroup(jfr_AlterarClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jfr_AlterarClienteLayout.createSequentialGroup()
                    .addComponent(pane_form3, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("StandBy");
        setMinimumSize(new java.awt.Dimension(1280, 720));
        setUndecorated(true);
        setSize(new java.awt.Dimension(1280, 720));
        getContentPane().setLayout(null);

        pane_TopSide2.setBackground(new java.awt.Color(102, 102, 102));
        pane_TopSide2.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                pane_TopSide2MouseDragged(evt);
            }
        });
        pane_TopSide2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pane_TopSide2MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                pane_TopSide2MouseReleased(evt);
            }
        });
        pane_TopSide2.setLayout(null);
        getContentPane().add(pane_TopSide2);
        pane_TopSide2.setBounds(90, 0, 1160, 30);

        pane_TopSide1.setBackground(new java.awt.Color(51, 51, 51));
        pane_TopSide1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                pane_TopSide1MouseDragged(evt);
            }
        });
        pane_TopSide1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pane_TopSide1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                pane_TopSide1MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout pane_TopSide1Layout = new javax.swing.GroupLayout(pane_TopSide1);
        pane_TopSide1.setLayout(pane_TopSide1Layout);
        pane_TopSide1Layout.setHorizontalGroup(
            pane_TopSide1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 90, Short.MAX_VALUE)
        );
        pane_TopSide1Layout.setVerticalGroup(
            pane_TopSide1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 30, Short.MAX_VALUE)
        );

        getContentPane().add(pane_TopSide1);
        pane_TopSide1.setBounds(0, 0, 90, 30);

        CardLayout.setLayout(new java.awt.CardLayout());

        pane_Ordens.setBackground(new java.awt.Color(102, 102, 102));
        pane_Ordens.setMinimumSize(new java.awt.Dimension(1060, 690));
        pane_Ordens.setPreferredSize(new java.awt.Dimension(1060, 690));
        pane_Ordens.setLayout(null);

        btnSAIR_ordens.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_ordens.setBorder(null);
        btnSAIR_ordens.setContentAreaFilled(false);
        btnSAIR_ordens.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_ordensActionPerformed(evt);
            }
        });
        pane_Ordens.add(btnSAIR_ordens);
        btnSAIR_ordens.setBounds(1160, 0, 30, 30);

        pane_Add_Entrada.setBackground(new java.awt.Color(102, 102, 102));

        lblAddEntrada.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAddEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_add_new_80px.png"))); // NOI18N
        lblAddEntrada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblAddEntradaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblAddEntradaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblAddEntradaMouseExited(evt);
            }
        });

        javax.swing.GroupLayout pane_Add_EntradaLayout = new javax.swing.GroupLayout(pane_Add_Entrada);
        pane_Add_Entrada.setLayout(pane_Add_EntradaLayout);
        pane_Add_EntradaLayout.setHorizontalGroup(
            pane_Add_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 90, Short.MAX_VALUE)
            .addGroup(pane_Add_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pane_Add_EntradaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblAddEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pane_Add_EntradaLayout.setVerticalGroup(
            pane_Add_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
            .addGroup(pane_Add_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pane_Add_EntradaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblAddEntrada)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pane_Ordens.add(pane_Add_Entrada);
        pane_Add_Entrada.setBounds(1080, 108, 90, 80);

        pane_Remove_Entrada.setBackground(new java.awt.Color(102, 102, 102));

        lblRemoveEntrada.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblRemoveEntrada.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_broom_80px_1.png"))); // NOI18N
        lblRemoveEntrada.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblRemoveEntradaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblRemoveEntradaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblRemoveEntradaMouseExited(evt);
            }
        });

        javax.swing.GroupLayout pane_Remove_EntradaLayout = new javax.swing.GroupLayout(pane_Remove_Entrada);
        pane_Remove_Entrada.setLayout(pane_Remove_EntradaLayout);
        pane_Remove_EntradaLayout.setHorizontalGroup(
            pane_Remove_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 90, Short.MAX_VALUE)
            .addGroup(pane_Remove_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pane_Remove_EntradaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblRemoveEntrada, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        pane_Remove_EntradaLayout.setVerticalGroup(
            pane_Remove_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 80, Short.MAX_VALUE)
            .addGroup(pane_Remove_EntradaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(pane_Remove_EntradaLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lblRemoveEntrada)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pane_Ordens.add(pane_Remove_Entrada);
        pane_Remove_Entrada.setBounds(1080, 190, 90, 80);

        pane_Servicos.setBackground(new java.awt.Color(51, 51, 51));

        lblOrdensTOPMENU.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblOrdensTOPMENU.setForeground(new java.awt.Color(209, 209, 209));
        lblOrdensTOPMENU.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrdensTOPMENU.setText("ORDENS DE SERVIÇO");

        javax.swing.GroupLayout pane_ServicosLayout = new javax.swing.GroupLayout(pane_Servicos);
        pane_Servicos.setLayout(pane_ServicosLayout);
        pane_ServicosLayout.setHorizontalGroup(
            pane_ServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pane_ServicosLayout.createSequentialGroup()
                .addComponent(lblOrdensTOPMENU, javax.swing.GroupLayout.DEFAULT_SIZE, 1140, Short.MAX_VALUE)
                .addContainerGap())
        );
        pane_ServicosLayout.setVerticalGroup(
            pane_ServicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pane_ServicosLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblOrdensTOPMENU, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pane_Ordens.add(pane_Servicos);
        pane_Servicos.setBounds(10, 30, 1150, 40);

        jtableOrdensServicos.setFont(new java.awt.Font("Malgun Gothic", 1, 12)); // NOI18N
        jtableOrdensServicos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"10/10/2019", "Adriano Andrade", "Samsung", "Sem Rede", "todo fudido"},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Data", "Nome", "Aparelho", "Defeito", "Situacao"
            }
        ));
        jtableOrdensServicos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtableOrdensServicosMouseClicked(evt);
            }
        });
        jtableOrdensServicos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtableOrdensServicosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jtableOrdensServicos);

        pane_Ordens.add(jScrollPane1);
        jScrollPane1.setBounds(10, 290, 1150, 420);

        txtAparelho.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        txtAparelho.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtAparelho.setBorder(null);
        txtAparelho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAparelhoActionPerformed(evt);
            }
        });
        txtAparelho.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtAparelhoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAparelhoKeyTyped(evt);
            }
        });
        pane_Ordens.add(txtAparelho);
        txtAparelho.setBounds(350, 160, 204, 30);

        txtDefeito.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        txtDefeito.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtDefeito.setBorder(null);
        txtDefeito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtDefeitoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDefeitoKeyTyped(evt);
            }
        });
        pane_Ordens.add(txtDefeito);
        txtDefeito.setBounds(590, 160, 264, 30);

        txtSituacao.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        txtSituacao.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSituacao.setBorder(null);
        txtSituacao.setNextFocusableComponent(jcmbNomeCliente);
        txtSituacao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSituacaoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSituacaoKeyTyped(evt);
            }
        });
        pane_Ordens.add(txtSituacao);
        txtSituacao.setBounds(8, 228, 1060, 30);

        txtSenha.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        txtSenha.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtSenha.setBorder(null);
        txtSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtSenhaKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSenhaKeyTyped(evt);
            }
        });
        pane_Ordens.add(txtSenha);
        txtSenha.setBounds(890, 160, 180, 30);

        lblVoltar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_undo_80px.png"))); // NOI18N
        lblVoltar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblVoltarMouseClicked(evt);
            }
        });
        pane_Ordens.add(lblVoltar);
        lblVoltar.setBounds(730, -140, 0, 0);

        jcmbNomeCliente.setBackground(new java.awt.Color(102, 102, 102));
        jcmbNomeCliente.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        jcmbNomeCliente.setForeground(new java.awt.Color(255, 0, 51));
        jcmbNomeCliente.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jcmbNomeCliente.setBorder(null);
        pane_Ordens.add(jcmbNomeCliente);
        jcmbNomeCliente.setBounds(9, 158, 305, 30);

        atras_situacao.setBackground(new java.awt.Color(51, 51, 51));

        lblSituacao.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblSituacao.setForeground(new java.awt.Color(209, 209, 209));
        lblSituacao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblSituacao.setText("SITUACAO");

        javax.swing.GroupLayout atras_situacaoLayout = new javax.swing.GroupLayout(atras_situacao);
        atras_situacao.setLayout(atras_situacaoLayout);
        atras_situacaoLayout.setHorizontalGroup(
            atras_situacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblSituacao, javax.swing.GroupLayout.DEFAULT_SIZE, 1057, Short.MAX_VALUE)
        );
        atras_situacaoLayout.setVerticalGroup(
            atras_situacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblSituacao, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        pane_Ordens.add(atras_situacao);
        atras_situacao.setBounds(10, 200, 1057, 30);

        atras_defeito.setBackground(new java.awt.Color(51, 51, 51));

        lblDefeito.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblDefeito.setForeground(new java.awt.Color(209, 209, 209));
        lblDefeito.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblDefeito.setText("DEFEITO");

        javax.swing.GroupLayout atras_defeitoLayout = new javax.swing.GroupLayout(atras_defeito);
        atras_defeito.setLayout(atras_defeitoLayout);
        atras_defeitoLayout.setHorizontalGroup(
            atras_defeitoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblDefeito, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
        );
        atras_defeitoLayout.setVerticalGroup(
            atras_defeitoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblDefeito, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        pane_Ordens.add(atras_defeito);
        atras_defeito.setBounds(591, 131, 261, 30);

        atras_aparelho.setBackground(new java.awt.Color(51, 51, 51));

        lblAparelho.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblAparelho.setForeground(new java.awt.Color(209, 209, 209));
        lblAparelho.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblAparelho.setText("APARELHO");

        javax.swing.GroupLayout atras_aparelhoLayout = new javax.swing.GroupLayout(atras_aparelho);
        atras_aparelho.setLayout(atras_aparelhoLayout);
        atras_aparelhoLayout.setHorizontalGroup(
            atras_aparelhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblAparelho, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
        );
        atras_aparelhoLayout.setVerticalGroup(
            atras_aparelhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblAparelho, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        pane_Ordens.add(atras_aparelho);
        atras_aparelho.setBounds(351, 131, 202, 30);

        atras_valor.setBackground(new java.awt.Color(51, 51, 51));

        lblValor.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblValor.setForeground(new java.awt.Color(209, 209, 209));
        lblValor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblValor.setText("SENHA");

        javax.swing.GroupLayout atras_valorLayout = new javax.swing.GroupLayout(atras_valor);
        atras_valor.setLayout(atras_valorLayout);
        atras_valorLayout.setHorizontalGroup(
            atras_valorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblValor, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE)
        );
        atras_valorLayout.setVerticalGroup(
            atras_valorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, atras_valorLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblValor, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pane_Ordens.add(atras_valor);
        atras_valor.setBounds(891, 131, 178, 30);

        atras_clienteCOMBO.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                atras_clienteCOMBOActionPerformed(evt);
            }
        });
        pane_Ordens.add(atras_clienteCOMBO);
        atras_clienteCOMBO.setBounds(9, 158, 264, 30);

        atras_cliente.setBackground(new java.awt.Color(51, 51, 51));

        lblCliente.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblCliente.setForeground(new java.awt.Color(209, 209, 209));
        lblCliente.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblCliente.setText("CLIENTE");

        javax.swing.GroupLayout atras_clienteLayout = new javax.swing.GroupLayout(atras_cliente);
        atras_cliente.setLayout(atras_clienteLayout);
        atras_clienteLayout.setHorizontalGroup(
            atras_clienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );
        atras_clienteLayout.setVerticalGroup(
            atras_clienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        pane_Ordens.add(atras_cliente);
        atras_cliente.setBounds(10, 130, 300, 30);

        txtBuscarServicosCliente.setFont(new java.awt.Font("Malgun Gothic Semilight", 1, 26)); // NOI18N
        txtBuscarServicosCliente.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtBuscarServicosCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtBuscarServicosClienteKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBuscarServicosClienteKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscarServicosClienteKeyTyped(evt);
            }
        });
        pane_Ordens.add(txtBuscarServicosCliente);
        txtBuscarServicosCliente.setBounds(70, 80, 1000, 40);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_google_web_search_80px.png"))); // NOI18N
        pane_Ordens.add(jLabel6);
        jLabel6.setBounds(10, 70, 60, 60);

        CardLayout.add(pane_Ordens, "ordens_servico");

        pane_Cad_Cliente.setBackground(new java.awt.Color(102, 102, 102));
        pane_Cad_Cliente.setMinimumSize(new java.awt.Dimension(1060, 690));
        pane_Cad_Cliente.setPreferredSize(new java.awt.Dimension(1060, 690));
        pane_Cad_Cliente.setLayout(null);

        btnSAIR_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_cliente.setBorder(null);
        btnSAIR_cliente.setContentAreaFilled(false);
        btnSAIR_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_clienteActionPerformed(evt);
            }
        });
        pane_Cad_Cliente.add(btnSAIR_cliente);
        btnSAIR_cliente.setBounds(1160, 0, 30, 30);

        lblNome.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblNome.setForeground(new java.awt.Color(240, 240, 240));
        lblNome.setText("Nome : ");
        pane_Cad_Cliente.add(lblNome);
        lblNome.setBounds(432, 40, 70, 25);

        lblCPF.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblCPF.setForeground(new java.awt.Color(240, 240, 240));
        lblCPF.setText("CPF : ");
        pane_Cad_Cliente.add(lblCPF);
        lblCPF.setBounds(450, 100, 50, 25);

        lblTelefone.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblTelefone.setForeground(new java.awt.Color(240, 240, 240));
        lblTelefone.setText("Telefone : ");
        pane_Cad_Cliente.add(lblTelefone);
        lblTelefone.setBounds(410, 160, 100, 25);

        txtNome.setFont(new java.awt.Font("Malgun Gothic Semilight", 0, 18)); // NOI18N
        txtNome.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtNome.setBorder(null);
        txtNome.setNextFocusableComponent(txtCPF);
        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNomeKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNomeKeyTyped(evt);
            }
        });
        pane_Cad_Cliente.add(txtNome);
        txtNome.setBounds(500, 40, 240, 30);

        txtTelefone.setFont(new java.awt.Font("Malgun Gothic Semilight", 0, 18)); // NOI18N
        txtTelefone.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTelefone.setBorder(null);
        txtTelefone.setNextFocusableComponent(txtProcurarCliente);
        txtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtTelefoneKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefoneKeyTyped(evt);
            }
        });
        pane_Cad_Cliente.add(txtTelefone);
        txtTelefone.setBounds(500, 160, 240, 30);
        pane_Cad_Cliente.add(jLabel1);
        jLabel1.setBounds(980, 640, 0, 0);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/mario run1.gif"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        pane_Cad_Cliente.add(jLabel2);
        jLabel2.setBounds(1110, 650, 80, 70);

        btnRegistrarCliente.setBackground(new java.awt.Color(102, 102, 102));
        btnRegistrarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/cl_registrar1.png"))); // NOI18N
        btnRegistrarCliente.setContentAreaFilled(false);
        btnRegistrarCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnRegistrarClienteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnRegistrarClienteMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                btnRegistrarClienteMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                btnRegistrarClienteMouseReleased(evt);
            }
        });
        btnRegistrarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarClienteActionPerformed(evt);
            }
        });
        pane_Cad_Cliente.add(btnRegistrarCliente);
        btnRegistrarCliente.setBounds(480, 210, 260, 70);

        jPanel2.setBackground(new java.awt.Color(80, 80, 80));

        jtableListaClientes.setFont(new java.awt.Font("Century Gothic", 0, 14)); // NOI18N
        jtableListaClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"1", "Adriano Andrade", "71992855114", "0715239651"},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Nome", "Telefone", "CPF"
            }
        ));
        jtableListaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtableListaClientesMouseClicked(evt);
            }
        });
        jtableListaClientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtableListaClientesKeyPressed(evt);
            }
        });
        jScrollPane2.setViewportView(jtableListaClientes);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pane_Cad_Cliente.add(jPanel2);
        jPanel2.setBounds(370, 330, 520, 380);

        txtProcurarCliente.setFont(new java.awt.Font("Malgun Gothic", 1, 16)); // NOI18N
        txtProcurarCliente.setNextFocusableComponent(txtNome);
        txtProcurarCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtProcurarClienteKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProcurarClienteKeyTyped(evt);
            }
        });
        pane_Cad_Cliente.add(txtProcurarCliente);
        txtProcurarCliente.setBounds(360, 290, 540, 30);

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_search_40px.png"))); // NOI18N
        pane_Cad_Cliente.add(jLabel3);
        jLabel3.setBounds(320, 285, 40, 40);

        try {
            txtCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCPF.setNextFocusableComponent(txtTelefone);
        txtCPF.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtCPFKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCPFKeyTyped(evt);
            }
        });
        pane_Cad_Cliente.add(txtCPF);
        txtCPF.setBounds(500, 100, 240, 30);

        CardLayout.add(pane_Cad_Cliente, "cad_cliente");

        pane_GastosLucros.setBackground(new java.awt.Color(102, 102, 102));
        pane_GastosLucros.setLayout(null);

        btnSAIR_removeCl.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_removeCl.setBorder(null);
        btnSAIR_removeCl.setContentAreaFilled(false);
        btnSAIR_removeCl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_removeClActionPerformed(evt);
            }
        });
        pane_GastosLucros.add(btnSAIR_removeCl);
        btnSAIR_removeCl.setBounds(1160, 0, 30, 30);

        jtable_GastosLucros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jtable_GastosLucros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtable_GastosLucrosMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(jtable_GastosLucros);

        pane_GastosLucros.add(jScrollPane4);
        jScrollPane4.setBounds(10, 70, 1150, 350);

        jtable_GastosPessoais.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Produto", "Valor"
            }
        ));
        jtable_GastosPessoais.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtable_GastosPessoaisMouseClicked(evt);
            }
        });
        jtable_GastosPessoais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jtable_GastosPessoaisKeyPressed(evt);
            }
        });
        jScrollPane5.setViewportView(jtable_GastosPessoais);

        pane_GastosLucros.add(jScrollPane5);
        jScrollPane5.setBounds(10, 470, 340, 240);

        jpaneGastos.setBackground(new java.awt.Color(51, 51, 51));
        jpaneGastos.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblGastosPessoaisText.setFont(new java.awt.Font("Malgun Gothic", 1, 18)); // NOI18N
        lblGastosPessoaisText.setForeground(new java.awt.Color(204, 204, 204));
        lblGastosPessoaisText.setText("Gastos c/ Assist: ");
        jpaneGastos.add(lblGastosPessoaisText, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 150, 20));

        lblGastosPessoais.setFont(new java.awt.Font("Calibri Light", 1, 25)); // NOI18N
        lblGastosPessoais.setForeground(new java.awt.Color(255, 51, 51));
        lblGastosPessoais.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblGastosPessoais.setText("0");
        jpaneGastos.add(lblGastosPessoais, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, 163, -1));

        lblLucroServicosText.setFont(new java.awt.Font("Malgun Gothic", 1, 18)); // NOI18N
        lblLucroServicosText.setForeground(new java.awt.Color(204, 204, 204));
        lblLucroServicosText.setText("Lucro Serviços:");
        jpaneGastos.add(lblLucroServicosText, new org.netbeans.lib.awtextra.AbsoluteConstraints(92, 50, 130, 20));

        lblLucrosServicosDone.setFont(new java.awt.Font("Calibri Light", 1, 25)); // NOI18N
        lblLucrosServicosDone.setForeground(new java.awt.Color(0, 102, 255));
        lblLucrosServicosDone.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLucrosServicosDone.setText("0");
        jpaneGastos.add(lblLucrosServicosDone, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 50, 161, 30));

        lblLucroTotalText.setFont(new java.awt.Font("Malgun Gothic", 1, 18)); // NOI18N
        lblLucroTotalText.setForeground(new java.awt.Color(204, 204, 204));
        lblLucroTotalText.setText("Lucro Total:");
        jpaneGastos.add(lblLucroTotalText, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 88, -1, 20));

        lblLucroTotal.setFont(new java.awt.Font("Calibri Light", 1, 25)); // NOI18N
        lblLucroTotal.setForeground(new java.awt.Color(0, 255, 51));
        lblLucroTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblLucroTotal.setText("0");
        jpaneGastos.add(lblLucroTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 90, 164, 30));
        jpaneGastos.add(separador1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 450, -1));
        jpaneGastos.add(separador2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 40, 450, -1));
        jpaneGastos.add(separador3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 450, -1));
        jpaneGastos.add(separador4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 118, 450, 10));

        pane_GastosLucros.add(jpaneGastos);
        jpaneGastos.setBounds(360, 470, 450, 120);

        lblProdutoGastosPessoais.setFont(new java.awt.Font("Malgun Gothic", 1, 18)); // NOI18N
        lblProdutoGastosPessoais.setForeground(new java.awt.Color(204, 204, 204));
        lblProdutoGastosPessoais.setText("Produto");
        pane_GastosLucros.add(lblProdutoGastosPessoais);
        lblProdutoGastosPessoais.setBounds(380, 610, 80, 30);

        txtProdutoGastosPessoais.setFont(new java.awt.Font("Malgun Gothic", 1, 14)); // NOI18N
        txtProdutoGastosPessoais.setNextFocusableComponent(txtValorGastosPessoais);
        txtProdutoGastosPessoais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtProdutoGastosPessoaisKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProdutoGastosPessoaisKeyTyped(evt);
            }
        });
        pane_GastosLucros.add(txtProdutoGastosPessoais);
        txtProdutoGastosPessoais.setBounds(460, 610, 250, 30);

        lblValorGastosPessoais.setFont(new java.awt.Font("Malgun Gothic", 1, 18)); // NOI18N
        lblValorGastosPessoais.setForeground(new java.awt.Color(204, 204, 204));
        lblValorGastosPessoais.setText("Valor R$");
        pane_GastosLucros.add(lblValorGastosPessoais);
        lblValorGastosPessoais.setBounds(380, 650, 80, 30);

        txtValorGastosPessoais.setFont(new java.awt.Font("Malgun Gothic", 1, 14)); // NOI18N
        txtValorGastosPessoais.setNextFocusableComponent(btnAddProdutoGastosPessoais);
        txtValorGastosPessoais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtValorGastosPessoaisKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValorGastosPessoaisKeyTyped(evt);
            }
        });
        pane_GastosLucros.add(txtValorGastosPessoais);
        txtValorGastosPessoais.setBounds(460, 650, 250, 30);

        btnAddProdutoGastosPessoais.setBackground(new java.awt.Color(90, 90, 90));
        btnAddProdutoGastosPessoais.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_save_close_70px.png"))); // NOI18N
        btnAddProdutoGastosPessoais.setNextFocusableComponent(txtProdutoGastosPessoais);
        btnAddProdutoGastosPessoais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddProdutoGastosPessoaisActionPerformed(evt);
            }
        });
        pane_GastosLucros.add(btnAddProdutoGastosPessoais);
        btnAddProdutoGastosPessoais.setBounds(730, 610, 70, 70);
        btnAddProdutoGastosPessoais.setOpaque(false);
        btnAddProdutoGastosPessoais.setContentAreaFilled(false);
        btnAddProdutoGastosPessoais.setBorderPainted(false);

        pane_Servicos3.setBackground(new java.awt.Color(51, 51, 51));

        lblOrdensTOPMENU3.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblOrdensTOPMENU3.setForeground(new java.awt.Color(209, 209, 209));
        lblOrdensTOPMENU3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrdensTOPMENU3.setText("GASTOS TEMP");

        javax.swing.GroupLayout pane_Servicos3Layout = new javax.swing.GroupLayout(pane_Servicos3);
        pane_Servicos3.setLayout(pane_Servicos3Layout);
        pane_Servicos3Layout.setHorizontalGroup(
            pane_Servicos3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOrdensTOPMENU3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
        );
        pane_Servicos3Layout.setVerticalGroup(
            pane_Servicos3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pane_Servicos3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblOrdensTOPMENU3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pane_GastosLucros.add(pane_Servicos3);
        pane_Servicos3.setBounds(820, 430, 340, 40);

        pane_Servicos4.setBackground(new java.awt.Color(51, 51, 51));

        lblOrdensTOPMENU5.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblOrdensTOPMENU5.setForeground(new java.awt.Color(209, 209, 209));
        lblOrdensTOPMENU5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrdensTOPMENU5.setText("TOTAL");

        javax.swing.GroupLayout pane_Servicos4Layout = new javax.swing.GroupLayout(pane_Servicos4);
        pane_Servicos4.setLayout(pane_Servicos4Layout);
        pane_Servicos4Layout.setHorizontalGroup(
            pane_Servicos4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pane_Servicos4Layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(lblOrdensTOPMENU5, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(70, Short.MAX_VALUE))
        );
        pane_Servicos4Layout.setVerticalGroup(
            pane_Servicos4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOrdensTOPMENU5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        pane_GastosLucros.add(pane_Servicos4);
        pane_Servicos4.setBounds(360, 430, 450, 40);

        pane_Servicos1.setBackground(new java.awt.Color(51, 51, 51));

        lblOrdensTOPMENU1.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblOrdensTOPMENU1.setForeground(new java.awt.Color(209, 209, 209));
        lblOrdensTOPMENU1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrdensTOPMENU1.setText("GASTOS");

        javax.swing.GroupLayout pane_Servicos1Layout = new javax.swing.GroupLayout(pane_Servicos1);
        pane_Servicos1.setLayout(pane_Servicos1Layout);
        pane_Servicos1Layout.setHorizontalGroup(
            pane_Servicos1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOrdensTOPMENU1, javax.swing.GroupLayout.DEFAULT_SIZE, 340, Short.MAX_VALUE)
        );
        pane_Servicos1Layout.setVerticalGroup(
            pane_Servicos1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pane_Servicos1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblOrdensTOPMENU1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pane_GastosLucros.add(pane_Servicos1);
        pane_Servicos1.setBounds(10, 430, 340, 40);

        pane_Servicos2.setBackground(new java.awt.Color(51, 51, 51));

        lblOrdensTOPMENU2.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblOrdensTOPMENU2.setForeground(new java.awt.Color(209, 209, 209));
        lblOrdensTOPMENU2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblOrdensTOPMENU2.setText("SERVIÇOS COMPLETOS");

        javax.swing.GroupLayout pane_Servicos2Layout = new javax.swing.GroupLayout(pane_Servicos2);
        pane_Servicos2.setLayout(pane_Servicos2Layout);
        pane_Servicos2Layout.setHorizontalGroup(
            pane_Servicos2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOrdensTOPMENU2, javax.swing.GroupLayout.DEFAULT_SIZE, 1150, Short.MAX_VALUE)
        );
        pane_Servicos2Layout.setVerticalGroup(
            pane_Servicos2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pane_Servicos2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(lblOrdensTOPMENU2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pane_GastosLucros.add(pane_Servicos2);
        pane_Servicos2.setBounds(10, 30, 1150, 40);

        jtable_GastosPessoaisTemporarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Title 1", "Title 2"
            }
        ));
        jtable_GastosPessoaisTemporarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtable_GastosPessoaisTemporariosMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(jtable_GastosPessoaisTemporarios);

        pane_GastosLucros.add(jScrollPane6);
        jScrollPane6.setBounds(820, 470, 340, 240);

        checkGastos.setFont(new java.awt.Font("Malgun Gothic Semilight", 1, 15)); // NOI18N
        checkGastos.setText("Gastos");
        checkGastos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkGastosMouseClicked(evt);
            }
        });
        pane_GastosLucros.add(checkGastos);
        checkGastos.setBounds(460, 690, 70, 20);

        checkGastosTemp.setFont(new java.awt.Font("Malgun Gothic Semilight", 1, 15)); // NOI18N
        checkGastosTemp.setText("Gastos TEMP");
        checkGastosTemp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkGastosTempMouseClicked(evt);
            }
        });
        pane_GastosLucros.add(checkGastosTemp);
        checkGastosTemp.setBounds(600, 690, 110, 20);

        CardLayout.add(pane_GastosLucros, "gastoslucros");

        pane_Extrato.setBackground(new java.awt.Color(102, 102, 102));
        pane_Extrato.setLayout(null);

        btnSAIR_removeCl1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_shutdown_25px.png"))); // NOI18N
        btnSAIR_removeCl1.setBorder(null);
        btnSAIR_removeCl1.setContentAreaFilled(false);
        btnSAIR_removeCl1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSAIR_removeCl1ActionPerformed(evt);
            }
        });
        pane_Extrato.add(btnSAIR_removeCl1);
        btnSAIR_removeCl1.setBounds(1160, 0, 30, 30);

        jPanel1.setBackground(new java.awt.Color(50, 50, 50));
        jPanel1.setLayout(null);

        jtable_Extrato.setFont(new java.awt.Font("Malgun Gothic Semilight", 0, 14)); // NOI18N
        jtable_Extrato.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"2020-03-14", " Adriano Fraga de Andrade", " Testando", " Testando", "TESTE",  new Float(10.0),  new Float(1000.0)},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Data", "Nome", "Aparelho", "Defeito", "Situacao", "Peca R$", "Servico R$"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Float.class, java.lang.Float.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane7.setViewportView(jtable_Extrato);

        jPanel1.add(jScrollPane7);
        jScrollPane7.setBounds(10, 170, 1140, 480);
        jPanel1.add(jdateInicial);
        jdateInicial.setBounds(20, 40, 200, 30);
        jPanel1.add(jdateFinal);
        jdateFinal.setBounds(20, 110, 200, 30);

        btnVerExtrato.setBackground(new java.awt.Color(0, 0, 70));
        btnVerExtrato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_search_80px.png"))); // NOI18N
        btnVerExtrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerExtratoActionPerformed(evt);
            }
        });
        jPanel1.add(btnVerExtrato);
        btnVerExtrato.setBounds(230, 40, 60, 100);

        jPanel5.setBackground(new java.awt.Color(0, 0, 102));

        jLabel4.setFont(new java.awt.Font("Malgun Gothic Semilight", 1, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(204, 204, 204));
        jLabel4.setText("Data Inicial:");

        jLabel5.setFont(new java.awt.Font("Malgun Gothic Semilight", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(204, 204, 204));
        jLabel5.setText("Data Final:");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(180, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 49, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel5);
        jPanel5.setBounds(10, 10, 290, 150);

        jPanel6.setBackground(new java.awt.Color(0, 0, 51));

        jpanelLucroExtrato.setBackground(new java.awt.Color(0, 0, 51));
        jpanelLucroExtrato.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 255), 3, true));

        lblLucroExtrato.setFont(new java.awt.Font("Malgun Gothic Semilight", 1, 24)); // NOI18N
        lblLucroExtrato.setForeground(new java.awt.Color(255, 255, 255));
        lblLucroExtrato.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLucroExtrato.setText("R$ 0.00");

        javax.swing.GroupLayout jpanelLucroExtratoLayout = new javax.swing.GroupLayout(jpanelLucroExtrato);
        jpanelLucroExtrato.setLayout(jpanelLucroExtratoLayout);
        jpanelLucroExtratoLayout.setHorizontalGroup(
            jpanelLucroExtratoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanelLucroExtratoLayout.createSequentialGroup()
                .addGap(205, 205, 205)
                .addComponent(lblLucroExtrato, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(187, Short.MAX_VALUE))
        );
        jpanelLucroExtratoLayout.setVerticalGroup(
            jpanelLucroExtratoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanelLucroExtratoLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(lblLucroExtrato, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(156, 156, 156)
                .addComponent(jpanelLucroExtrato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(157, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jpanelLucroExtrato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel6);
        jPanel6.setBounds(310, 10, 840, 150);

        pane_Extrato.add(jPanel1);
        jPanel1.setBounds(10, 40, 1160, 660);

        CardLayout.add(pane_Extrato, "extrato");

        getContentPane().add(CardLayout);
        CardLayout.setBounds(90, 0, 1190, 720);

        pane_Menu.setBackground(new java.awt.Color(51, 51, 51));

        menu1_service.setBackground(new java.awt.Color(102, 102, 102));

        lblOrdensServico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_f1_key_80px.png"))); // NOI18N
        lblOrdensServico.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOrdensServicoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblOrdensServicoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblOrdensServicoMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblOrdensServicoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblOrdensServicoMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout menu1_serviceLayout = new javax.swing.GroupLayout(menu1_service);
        menu1_service.setLayout(menu1_serviceLayout);
        menu1_serviceLayout.setHorizontalGroup(
            menu1_serviceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu1_serviceLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblOrdensServico)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        menu1_serviceLayout.setVerticalGroup(
            menu1_serviceLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOrdensServico, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        menu2_register.setBackground(new java.awt.Color(51, 51, 51));
        menu2_register.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                menu2_registerMouseEntered(evt);
            }
        });

        lblCad_Cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_f2_key_80px.png"))); // NOI18N
        lblCad_Cliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCad_ClienteMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblCad_ClienteMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblCad_ClienteMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblCad_ClienteMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblCad_ClienteMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout menu2_registerLayout = new javax.swing.GroupLayout(menu2_register);
        menu2_register.setLayout(menu2_registerLayout);
        menu2_registerLayout.setHorizontalGroup(
            menu2_registerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu2_registerLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCad_Cliente)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        menu2_registerLayout.setVerticalGroup(
            menu2_registerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblCad_Cliente, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        menu3_doneServices.setBackground(new java.awt.Color(51, 51, 51));

        lblServicos_Done.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_f3_key_80px.png"))); // NOI18N
        lblServicos_Done.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblServicos_DoneMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblServicos_DoneMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblServicos_DoneMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblServicos_DoneMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblServicos_DoneMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout menu3_doneServicesLayout = new javax.swing.GroupLayout(menu3_doneServices);
        menu3_doneServices.setLayout(menu3_doneServicesLayout);
        menu3_doneServicesLayout.setHorizontalGroup(
            menu3_doneServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(menu3_doneServicesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblServicos_Done)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        menu3_doneServicesLayout.setVerticalGroup(
            menu3_doneServicesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblServicos_Done, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        menu4_orcament.setBackground(new java.awt.Color(51, 51, 51));

        lblOrcamento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_f4_key_80px.png"))); // NOI18N
        lblOrcamento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblOrcamentoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblOrcamentoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblOrcamentoMouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lblOrcamentoMousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lblOrcamentoMouseReleased(evt);
            }
        });

        javax.swing.GroupLayout menu4_orcamentLayout = new javax.swing.GroupLayout(menu4_orcament);
        menu4_orcament.setLayout(menu4_orcamentLayout);
        menu4_orcamentLayout.setHorizontalGroup(
            menu4_orcamentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menu4_orcamentLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblOrcamento)
                .addContainerGap())
        );
        menu4_orcamentLayout.setVerticalGroup(
            menu4_orcamentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblOrcamento, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        menu5_extrato.setBackground(new java.awt.Color(51, 51, 51));
        menu5_extrato.setPreferredSize(new java.awt.Dimension(100, 80));

        lblExtrato.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icones/icons8_f5_key_80px.png"))); // NOI18N
        lblExtrato.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblExtratoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout menu5_extratoLayout = new javax.swing.GroupLayout(menu5_extrato);
        menu5_extrato.setLayout(menu5_extratoLayout);
        menu5_extratoLayout.setHorizontalGroup(
            menu5_extratoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, menu5_extratoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblExtrato, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        menu5_extratoLayout.setVerticalGroup(
            menu5_extratoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblExtrato, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        divisoria.setPreferredSize(new java.awt.Dimension(60, 2));

        javax.swing.GroupLayout divisoriaLayout = new javax.swing.GroupLayout(divisoria);
        divisoria.setLayout(divisoriaLayout);
        divisoriaLayout.setHorizontalGroup(
            divisoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        divisoriaLayout.setVerticalGroup(
            divisoriaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 2, Short.MAX_VALUE)
        );

        lblLogoStandby.setFont(new java.awt.Font("Malgun Gothic", 0, 18)); // NOI18N
        lblLogoStandby.setForeground(new java.awt.Color(209, 209, 209));
        lblLogoStandby.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLogoStandby.setText("StandBy");
        lblLogoStandby.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblLogoStandbyMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pane_MenuLayout = new javax.swing.GroupLayout(pane_Menu);
        pane_Menu.setLayout(pane_MenuLayout);
        pane_MenuLayout.setHorizontalGroup(
            pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pane_MenuLayout.createSequentialGroup()
                .addGroup(pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pane_MenuLayout.createSequentialGroup()
                        .addGroup(pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(menu2_register, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(menu1_service, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(menu3_doneServices, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(menu4_orcament, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pane_MenuLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(lblLogoStandby, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pane_MenuLayout.createSequentialGroup()
                                    .addGap(10, 10, 10)
                                    .addComponent(divisoria, javax.swing.GroupLayout.DEFAULT_SIZE, 72, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pane_MenuLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(menu5_extrato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        pane_MenuLayout.setVerticalGroup(
            pane_MenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pane_MenuLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(lblLogoStandby, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(divisoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(menu1_service, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(menu2_register, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(menu3_doneServices, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(menu4_orcament, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addComponent(menu5_extrato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(pane_Menu);
        pane_Menu.setBounds(0, 0, 90, 720);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    public void cpfMASK() {
        MaskFormatter CPFMask;
        try {
            CPFMask = new MaskFormatter("###.###.###-##");
            txtCPF.setFormatterFactory(new DefaultFormatterFactory(CPFMask));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void cheatAbrirForm() {
        load.setVisible(true);
        try {
            sleep(3000);
        } catch (Exception e) {
        }
        load.setVisible(false);
        //this.setVisible(true);
    }

    public void preencherTodasTabelas() {
        p.preencherTableServicos();
        p.preencherTableGastosLucros();
        p.preencherRemoveClientes();
        p.preencherTableGastosPessoais();
        p.preencherTableGastosPessoaisTemporarios();
        p.preencherTableExtrato("", "");
        preencherComboboxCliente();
        carregarLabelsGastosLucros();
        preencherOrcamentos();

        lblLucroExtrato.setForeground(Color.white);
        Border lineBorder = BorderFactory.createLineBorder(Color.white, 3, true);
        TitledBorder title = BorderFactory.createTitledBorder(lineBorder, "");
        jpanelLucroExtrato.setBorder(title);
        lblLucroExtrato.setText("R$ 0.00");
    }

    public void carregarLabelsGastosLucros() {
        BigDecimal gastosPessoais = b.buscarGastosPessoaisTotais();
        BigDecimal lucrosServicos = b.buscarLucrosServicosProntos();
        BigDecimal lucroTotal = new BigDecimal(0);

        lucroTotal = lucrosServicos.subtract(gastosPessoais);

        lblGastosPessoais.setText("R$ " + b.buscarGastosPessoaisTotais().toString());
        lblLucrosServicosDone.setText("R$ " + b.buscarLucrosServicosProntos().toString());
        lblLucroTotal.setText("R$ " + lucroTotal);
    }

    public void adicionarPecaNova() {
        BigDecimal Servico = new BigDecimal(txtServicoAddPecas.getText());
        BigDecimal Pecas = new BigDecimal(txtPecaAddPecas.getText());
        try {
            i.inserirOrcamento(txtMarcaAddPecas.getText(), txtModeloAddPEcas.getText(), Servico, Pecas);
            JOptionPane.showMessageDialog(null, "Item adicionado com Sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            p_orc.preencherOrcamentos(txtMarcaAddPecas.getText());

            //limpar campos
            txtMarcaAddPecas.setText("");
            txtModeloAddPEcas.setText("");
            txtServicoAddPecas.setText("");
            txtPecaAddPecas.setText("");

            jfr_Orc_AddPecas.dispose();

        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Erro ao adicionar um novo item\n\n" + e + "", "ERRO AO ADICIONAR", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void PegarTeclaPressionada() {
        EventQueue queue = new EventQueue() {
            protected void dispatchEvent(AWTEvent event) {
                super.dispatchEvent(event);
                String a[];
                String tecla[];
                CardLayout cl = (CardLayout) CardLayout.getLayout();

                if (!event.paramString().equals("")) {
                    if (event.paramString().substring(0, 5).equals("KEY_P")) {
                        a = event.paramString().split(",");
                        tecla = a[1].split("=");
                        //System.out.println(tecla[1]);

                        switch (Integer.parseInt(tecla[1])) {

//                            case 9: //TECLA TAB
//
//                            case 10:
//                                //digite ação
//                                break;
//
//                            case 38:
//                                //digite ação
//                                break;
//
//                            case 40:
//                                //digite ação
//                                break;
                            case 112: //F1
                                cl.show(CardLayout, "ordens_servico");
                                attStatus.atualizarStatusCadServ();
                                //txtAparelho.requestFocus();
                                jcmbNomeCliente.requestFocus();
                                jdateInicial.setCalendar(null);
                                jdateFinal.setCalendar(null);
                                preencherTodasTabelas();
                                break;

                            case 27: //ESC
                                //digite ação
                                jFrame1.dispose();
                                jfr_Orcamento.dispose();
                                jfr_Orc_AddPecas.dispose();
                                jfr_AlterarCliente.dispose();
                                preencherTodasTabelas();
                                break;

                            case 113: //F2
                                cl.show(CardLayout, "cad_cliente");
                                attStatus.atualizarStatusCadCliente();
                                txtNome.requestFocus();
                                jdateInicial.setCalendar(null);
                                jdateFinal.setCalendar(null);
                                preencherTodasTabelas();
                                break;

                            case 114: //F3
                                cl.show(CardLayout, "gastoslucros");
                                attStatus.atualizarStatusServProntos();
                                txtProdutoGastosPessoais.requestFocus();
                                jdateInicial.setCalendar(null);
                                jdateFinal.setCalendar(null);
                                preencherTodasTabelas();
                                break;

                            case 115: //F4
                                jfr_Orcamento.setVisible(true);
                                jfr_Orcamento.setLocationRelativeTo(null);
                                attStatus.atualizarStatusOrcamento();
                                jdateInicial.setCalendar(null);
                                jdateFinal.setCalendar(null);
                                preencherTodasTabelas();
                                break;

                            case 116: //F5
                                cl.show(CardLayout, "extrato");
                                attStatus.atualizarStatusExtrato();
                                //txtProdutoGastosPessoais.requestFocus();
                                break;
//
//                            case 117: //F6
//                            //digite ação
//
//                            case 118: //F7
//                            //digite ação
//
//                            case 119: //F8
//                            //digite ação
//
//                            case 120: //F9
//                            //digite ação
//
//                            case 121: //F10
//                            //digite ação
//
//                            case 122: //F11
//                            //digite ação
//
//                            case 123: //F12
//                                //digite ação
//                                break;
                            default:
                                break;
                        }
                    }
                }
            }
        };
        Toolkit.getDefaultToolkit().getSystemEventQueue().push(queue);
    }

    public void teclaEnterRegistrarCliente() {
        inserirDados ins = new inserirDados();
        if (txtNome.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Preencha o Nome (Telefone opcional)", "CAMPOS VAZIOS", JOptionPane.WARNING_MESSAGE);
        } else {

            int duplicidade = b.verificarDuplicidadeCPF(txtCPF.getText());

            if (duplicidade == 0) {
                try {
                    ins.inserirCliente(txtNome.getText(), txtTelefone.getText(), txtCPF.getText());
                    JOptionPane.showMessageDialog(this, "Cliente cadastrado com sucesso!", "SUCESSO!", JOptionPane.INFORMATION_MESSAGE);

                    txtNome.setText("");
                    txtCPF.setText("");
                    txtTelefone.setText("");

                    p.preencherTableServicos();
                    p.preencherComboboxCliente();
                    p.preencherRemoveClientes();
                    CardLayout cl = (CardLayout) CardLayout.getLayout();
                    cl.show(CardLayout, "ordens_servico");

                    menu1_service.setBackground(new Color(102, 102, 102));
                    attStatus.atualizarStatusCadServ();

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, "ERROR\n\n" + e, "ERROR", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                txtCPF.setText("");
                txtCPF.requestFocus();
            }
        }
    }

    public void teclaEnterOrdemServ() {
        //|| txtSenha.getText().isEmpty()
        if (txtAparelho.getText().isEmpty() || txtDefeito.getText().isEmpty() || txtSituacao.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Existem campos vazios, favor preencher todos!", "CAMPOS VAZIOS", JOptionPane.WARNING_MESSAGE);
        } else {
            String nome = jcmbNomeCliente.getSelectedItem().toString();
            buscarDados bd = new buscarDados();
            try {
                int idCliente = bd.buscarIdCliente(nome);
                i.inserirServico(idCliente, txtAparelho.getText(), txtDefeito.getText(), txtSituacao.getText(), txtSenha.getText());
                p.preencherTableServicos();

                // Limpar Campos apos o Cadastro
                txtAparelho.setText("");
                txtDefeito.setText("");
                txtSituacao.setText("");
                txtSenha.setText("");
                txtAparelho.requestFocus();

                JOptionPane.showMessageDialog(this, "Serviço cadastrado com Sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Erro ao adicionar um novo servico\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void calcularLucro() {
        BigDecimal valorservico = new BigDecimal(txtValorServicoEdit.getText());
        BigDecimal valorpeca = new BigDecimal(txtValorPecaEdit.getText());

        if (txtValorServicoEdit.getText() == "100") {

            BigDecimal lucro = valorservico.subtract(valorpeca);
            //txtLucroEdit.setText("-" + valorpeca.toString());
            txtLucroEdit.setText("SEM LUCRO");

        } else {

            BigDecimal lucro = valorservico.subtract(valorpeca);
            txtLucroEdit.setText(lucro.toString());

        }

        BigDecimal lucro = new BigDecimal(txtLucroEdit.getText());
        if (lucro.signum() == -1) {
            txtLucroEdit.setForeground(Color.red);
        } else if (lucro.signum() == 0) {
            txtLucroEdit.setForeground(Color.black);
        } else {
            txtLucroEdit.setForeground(Color.green);
        }
    }

    public void preencherOrcamentos() {
//        try {
        p_orc.preencherOrcamentos("SAMSUNG");
//            p_orc.preencherSamsung();
//            p_orc.preencherMotorola();
//            p_orc.preencherLG();
//            p_orc.preencherIphone();
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(this, "Erro ao preencher os orcamentos : \n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
//        }
    }

    public void preencherComboboxCliente() {
        try {
            p.preencherComboboxCliente();
        } catch (SQLException | ClassNotFoundException e) {
            JOptionPane.showMessageDialog(this, "Nao foi possivel preencher a lista dos clientes\n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void pane_TopSide2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pane_TopSide2MousePressed
        setOpacity((float) 0.8);
        xx = evt.getX();
        yy = evt.getY();
    }//GEN-LAST:event_pane_TopSide2MousePressed

    private void pane_TopSide2MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pane_TopSide2MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xx, y - yy);
    }//GEN-LAST:event_pane_TopSide2MouseDragged

    private void pane_TopSide2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pane_TopSide2MouseReleased
        setOpacity((float) 1.0);
    }//GEN-LAST:event_pane_TopSide2MouseReleased

    private void pane_TopSide1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pane_TopSide1MousePressed
        setOpacity((float) 0.8);
        xx = evt.getX();
        yy = evt.getY();
    }//GEN-LAST:event_pane_TopSide1MousePressed

    private void pane_TopSide1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pane_TopSide1MouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        this.setLocation(x - xx, y - yy);
    }//GEN-LAST:event_pane_TopSide1MouseDragged

    private void pane_TopSide1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pane_TopSide1MouseReleased
        setOpacity((float) 1.0);
    }//GEN-LAST:event_pane_TopSide1MouseReleased

    private void lblOrdensServicoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrdensServicoMouseEntered
//        menu1_service.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblOrdensServicoMouseEntered

    private void lblOrdensServicoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrdensServicoMouseExited
//        menu1_service.setBackground(new Color(51, 51, 51));
    }//GEN-LAST:event_lblOrdensServicoMouseExited

    private void lblCad_ClienteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCad_ClienteMouseEntered
//        menu2_register.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblCad_ClienteMouseEntered

    private void lblCad_ClienteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCad_ClienteMouseExited
//        menu2_register.setBackground(new Color(51, 51, 51));
    }//GEN-LAST:event_lblCad_ClienteMouseExited

    private void lblServicos_DoneMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblServicos_DoneMouseEntered
//        menu3_doneServices.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblServicos_DoneMouseEntered

    private void lblServicos_DoneMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblServicos_DoneMouseExited
//        menu3_doneServices.setBackground(new Color(51, 51, 51));
    }//GEN-LAST:event_lblServicos_DoneMouseExited

    private void lblOrcamentoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrcamentoMouseEntered
//        menu4_orcament.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblOrcamentoMouseEntered

    private void lblOrcamentoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrcamentoMouseExited
//        menu4_orcament.setBackground(new Color(51, 51, 51));
    }//GEN-LAST:event_lblOrcamentoMouseExited

    private void menu2_registerMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menu2_registerMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_menu2_registerMouseEntered

    private void lblAddEntradaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAddEntradaMouseEntered
        pane_Add_Entrada.setBackground(new Color(51, 51, 51));
    }//GEN-LAST:event_lblAddEntradaMouseEntered

    private void lblAddEntradaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAddEntradaMouseExited
        pane_Add_Entrada.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblAddEntradaMouseExited

    private void lblRemoveEntradaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblRemoveEntradaMouseEntered
        pane_Remove_Entrada.setBackground(new Color(51, 51, 51));
    }//GEN-LAST:event_lblRemoveEntradaMouseEntered

    private void lblRemoveEntradaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblRemoveEntradaMouseExited
        pane_Remove_Entrada.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblRemoveEntradaMouseExited

    private void lblOrdensServicoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrdensServicoMousePressed
        menu1_service.setBackground(new Color(210, 210, 210));
    }//GEN-LAST:event_lblOrdensServicoMousePressed

    private void lblOrdensServicoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrdensServicoMouseReleased
        menu1_service.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblOrdensServicoMouseReleased

    private void lblOrdensServicoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrdensServicoMouseClicked
        CardLayout cl = (CardLayout) CardLayout.getLayout();
        cl.show(CardLayout, "ordens_servico");
        jdateInicial.setCalendar(null);
        jdateFinal.setCalendar(null);
        attStatus.atualizarStatusCadServ();
        preencherTodasTabelas();
    }//GEN-LAST:event_lblOrdensServicoMouseClicked

    private void lblCad_ClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCad_ClienteMouseClicked
        // TODO add your handling code here:
        CardLayout cl = (CardLayout) CardLayout.getLayout();
        cl.show(CardLayout, "cad_cliente");
        jdateInicial.setCalendar(null);
        jdateFinal.setCalendar(null);
        attStatus.atualizarStatusCadCliente();
        preencherTodasTabelas();
    }//GEN-LAST:event_lblCad_ClienteMouseClicked

    private void lblAddEntradaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAddEntradaMouseClicked
        teclaEnterOrdemServ();
    }//GEN-LAST:event_lblAddEntradaMouseClicked

    private void lblVoltarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblVoltarMouseClicked
    }//GEN-LAST:event_lblVoltarMouseClicked

    private void txtAparelhoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAparelhoActionPerformed
    }//GEN-LAST:event_txtAparelhoActionPerformed

    private void jtableOrdensServicosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtableOrdensServicosMouseClicked
        int linha = 0;
//        try {
        linha = jtableOrdensServicos.getSelectedRow();
        if (linha == -1) {
            JOptionPane.showMessageDialog(null, "Selecione primeiro UM click esquerdo do mouse o cliente\n"
                    + "que deseja remover, depois click com o direito para remover.", "!!!!!", JOptionPane.WARNING_MESSAGE);
        } else {
            txtDataEdit.setText(jtableOrdensServicos.getValueAt(linha, 0).toString());
            txtNomeEdit.setText(jtableOrdensServicos.getValueAt(linha, 1).toString());
            txtNomeEdit.setEditable(false);
            txtNomeEdit.setForeground(Color.red);
            txtAparelhoEdit.setText(jtableOrdensServicos.getValueAt(linha, 2).toString());
            txtDefeitoEdit.setText(jtableOrdensServicos.getValueAt(linha, 3).toString());
            txtPaneSituacaoEdit.setText(jtableOrdensServicos.getValueAt(linha, 4).toString());
            txtSenhaEdit.setText(jtableOrdensServicos.getValueAt(linha, 5).toString());
            txtValorPecaEdit.setText(jtableOrdensServicos.getValueAt(linha, 6).toString());
            txtValorServicoEdit.setText(jtableOrdensServicos.getValueAt(linha, 10).toString());
            txtLucroEdit.setEditable(false);
            txtLucroEdit.setText(jtableOrdensServicos.getValueAt(linha, 7).toString());
            lblSV_ID.setText(jtableOrdensServicos.getValueAt(linha, 9).toString());
            txtServicoEdit.setText("");
            txtServicoEdit.setForeground(Color.red);

            if (evt.getClickCount() == 2) {
                jFrame1.setVisible(true);
                jFrame1.setLocationRelativeTo(null);

                BigDecimal lucro = new BigDecimal(txtLucroEdit.getText());
                if (lucro.signum() == -1) {
                    txtLucroEdit.setForeground(Color.red);
                } else if (lucro.signum() == 0) {
                    txtLucroEdit.setForeground(Color.black);
                } else {
                    txtLucroEdit.setForeground(Color.green);
                }
                txtAparelhoEdit.requestFocus();

            } else if (evt.getButton() == MouseEvent.BUTTON3) {
//                int id = Integer.parseInt(jtableOrdensServicos.getValueAt(linha, 9).toString());
//                int retorno = decisaoDeletar("Deseja deletar este servico?");
//                if (retorno == 0) {
//                    d.deletarServicos(id);
//                    // Atualizar lista.
//                    p.preencherTableServicos();
//                }
            }
        }
//        } catch (Exception e) {
        //JOptionPane.showMessageDialog(null, "Selecione uma linha primeiro!", "!!!!!", JOptionPane.OK_OPTION);
//        }
    }//GEN-LAST:event_jtableOrdensServicosMouseClicked

    private void btnSAIR_ordensActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_ordensActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSAIR_ordensActionPerformed

    private void btnSAIR_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_clienteActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSAIR_clienteActionPerformed

    public static int decisaoDeletar(String messagem) {
        int result = JOptionPane.showConfirmDialog((Component) null, messagem, "ALERTA", JOptionPane.OK_CANCEL_OPTION);
        return result;
    }

    private void lblRemoveEntradaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblRemoveEntradaMouseClicked
        txtAparelho.setText("");
        txtDefeito.setText("");
        txtSituacao.setText("");
        txtSenha.setText("");

    }//GEN-LAST:event_lblRemoveEntradaMouseClicked

    private void jtableListaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtableListaClientesMouseClicked
        int linha = 0;
        linha = jtableListaClientes.getSelectedRow();

        if (linha == -1) {
            JOptionPane.showMessageDialog(null, "Selecione primeiro UM click esquerdo do mouse "
                    + "o cliente\nque deseja remover, depois click com o direito para remover.",
                    "!!!!!", JOptionPane.WARNING_MESSAGE);
        } else if (evt.getButton() == MouseEvent.BUTTON3) {
            {
                int id = Integer.parseInt(jtableListaClientes.getValueAt(linha, 0).toString());
                int retorno = decisaoDeletar("Deseja deletar este cliente?");
                if (retorno == 0) {
                    try {
                        d.deletarClientes(id);
                        // Atualizar lista.
                        p.preencherRemoveClientes();
                        p.preencherComboboxCliente();
                    } catch (SQLException | ClassNotFoundException ex) {
                        Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        } else if (evt.getClickCount() == 2) {
            jfr_AlterarCliente.setVisible(true);
            jfr_AlterarCliente.setLocationRelativeTo(null);
            //btnEditarCliente.setBackground(new Color(0,0,0,0));
            txtNomeClienteEdit.requestFocus();
            lblIdClienteEdit.setText(jtableListaClientes.getValueAt(linha, 0).toString());
            txtNomeClienteEdit.setText(jtableListaClientes.getValueAt(linha, 1).toString());
            txtTelefoneClienteEdit.setText(jtableListaClientes.getValueAt(linha, 2).toString());
            txtCpfClienteEdit.setText(jtableListaClientes.getValueAt(linha, 3).toString());

        }
    }//GEN-LAST:event_jtableListaClientesMouseClicked

    private void btnSAIR_removeClActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_removeClActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSAIR_removeClActionPerformed

    private void btnSAIR_form1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_form1ActionPerformed
        jFrame1.dispose();
    }//GEN-LAST:event_btnSAIR_form1ActionPerformed

    private void topsideMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topsideMousePressed
        jFrame1.setOpacity((float) 0.8);
        xx = evt.getX();
        yy = evt.getY();
    }//GEN-LAST:event_topsideMousePressed

    private void topsideMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topsideMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        jFrame1.setLocation(x - xx, y - yy);
    }//GEN-LAST:event_topsideMouseDragged

    private void topsideMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topsideMouseReleased
        jFrame1.setOpacity((float) 1.0);
    }//GEN-LAST:event_topsideMouseReleased

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        JOptionPane.showMessageDialog(this, "Alisson e viado", "EasterEgg", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jLabel2MouseClicked

    private void atras_clienteCOMBOActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atras_clienteCOMBOActionPerformed
    }//GEN-LAST:event_atras_clienteCOMBOActionPerformed

    private void btnSAIR_form2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_form2ActionPerformed
        jfr_Orcamento.dispose();
    }//GEN-LAST:event_btnSAIR_form2ActionPerformed

    private void lblOrcamentoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrcamentoMouseClicked
        jfr_Orcamento.setVisible(true);
        jfr_Orcamento.setLocationRelativeTo(null);
        jdateInicial.setCalendar(null);
        jdateFinal.setCalendar(null);
        attStatus.atualizarStatusOrcamento();
        preencherTodasTabelas();
    }//GEN-LAST:event_lblOrcamentoMouseClicked

    private void topside_orcamentoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topside_orcamentoMousePressed
        jfr_Orcamento.setOpacity((float) 0.8);
        xx = evt.getX();
        yy = evt.getY();
    }//GEN-LAST:event_topside_orcamentoMousePressed

    private void topside_orcamentoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topside_orcamentoMouseReleased
        jfr_Orcamento.setOpacity((float) 1.0);
    }//GEN-LAST:event_topside_orcamentoMouseReleased

    private void topside_orcamentoMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topside_orcamentoMouseDragged
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        jfr_Orcamento.setLocation(x - xx, y - yy);
    }//GEN-LAST:event_topside_orcamentoMouseDragged

    public void limparCamposEdicaoServico() {
        txtDataEdit.setText("");
        txtValorPecaEdit.setText("");
        txtValorServicoEdit.setText("");
        txtServicoEdit.setText("");
        txtSituacao.setText("");
    }
    private void lblCad_ClienteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCad_ClienteMousePressed
        menu2_register.setBackground(new Color(210, 210, 210));
    }//GEN-LAST:event_lblCad_ClienteMousePressed

    private void lblCad_ClienteMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCad_ClienteMouseReleased
        menu2_register.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblCad_ClienteMouseReleased

    private void lblServicos_DoneMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblServicos_DoneMousePressed
        menu3_doneServices.setBackground(new Color(210, 210, 210));
    }//GEN-LAST:event_lblServicos_DoneMousePressed

    private void lblServicos_DoneMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblServicos_DoneMouseReleased
        menu3_doneServices.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblServicos_DoneMouseReleased

    private void lblOrcamentoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrcamentoMousePressed
        menu4_orcament.setBackground(new Color(210, 210, 210));
    }//GEN-LAST:event_lblOrcamentoMousePressed

    private void lblOrcamentoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblOrcamentoMouseReleased
        //menu4_orcament.setBackground(new Color(102, 102, 102));
    }//GEN-LAST:event_lblOrcamentoMouseReleased

    private void btnRegistrarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarClienteActionPerformed
        teclaEnterRegistrarCliente();
    }//GEN-LAST:event_btnRegistrarClienteActionPerformed

    private void btnRegistrarClienteMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRegistrarClienteMouseEntered
        try {
            ImageIcon valid = new ImageIcon(getClass().getResource("cl_registrar2.png"));
            this.btnRegistrarCliente.setIcon(valid);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "ERRO: \n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnRegistrarClienteMouseEntered

    private void btnRegistrarClienteMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRegistrarClienteMouseExited
        try {
            ImageIcon valid = new ImageIcon(getClass().getResource("cl_registrar1.png"));
            this.btnRegistrarCliente.setIcon(valid);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnRegistrarClienteMouseExited

    private void btnRegistrarClienteMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRegistrarClienteMousePressed
        try {
            ImageIcon valid = new ImageIcon(getClass().getResource("cl_registrar3.png"));
            this.btnRegistrarCliente.setIcon(valid);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "ERRO: \n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnRegistrarClienteMousePressed

    private void btnRegistrarClienteMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRegistrarClienteMouseReleased
        try {
            ImageIcon valid = new ImageIcon(getClass().getResource("cl_registrar2.png"));
            this.btnRegistrarCliente.setIcon(valid);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "ERRO: \n\n" + e, "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnRegistrarClienteMouseReleased

    private void lblServicos_DoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblServicos_DoneMouseClicked
        CardLayout cl = (CardLayout) CardLayout.getLayout();
        cl.show(CardLayout, "gastoslucros");
        jdateInicial.setCalendar(null);
        jdateFinal.setCalendar(null);
        attStatus.atualizarStatusServProntos();
        preencherTodasTabelas();
    }//GEN-LAST:event_lblServicos_DoneMouseClicked

    private void lblSamsungMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSamsungMousePressed
        jpanel_samsung.setBackground(new Color(52, 14, 140));
    }//GEN-LAST:event_lblSamsungMousePressed

    private void lblSamsungMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSamsungMouseReleased
        jpanel_samsung.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblSamsungMouseReleased

    private void lblMotorolaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMotorolaMousePressed
        jpanel_motorola.setBackground(new Color(114, 6, 132));
    }//GEN-LAST:event_lblMotorolaMousePressed

    private void lblMotorolaMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMotorolaMouseReleased
        jpanel_motorola.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblMotorolaMouseReleased

    private void lblLGMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLGMousePressed
        jpanel_LG.setBackground(new Color(206, 0, 102));
    }//GEN-LAST:event_lblLGMousePressed

    private void lblLGMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLGMouseReleased
        jpanel_LG.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblLGMouseReleased

    private void lblIphoneMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIphoneMousePressed
        jpanel_iphone.setBackground(new Color(52, 126, 127));
        if (evt.getClickCount() == 2) {
            JOptionPane.showMessageDialog(null, "!!!!!", "teste", JOptionPane.OK_OPTION);
        }
    }//GEN-LAST:event_lblIphoneMousePressed

    private void lblIphoneMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIphoneMouseReleased
        jpanel_iphone.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblIphoneMouseReleased

    private void lblSamsungMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblSamsungMouseClicked
//        try {
        //p_orc.preencherSamsung();
        p_orc.preencherOrcamentos("SAMSUNG");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblSamsungMouseClicked

    private void lblMotorolaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblMotorolaMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("MOTOROLA");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblMotorolaMouseClicked

    private void btn_AddSamsungActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddSamsungActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("SAMSUNG");
    }//GEN-LAST:event_btn_AddSamsungActionPerformed

    private void btnSAIR_AddPecasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_AddPecasActionPerformed
        jfr_Orc_AddPecas.dispose();
    }//GEN-LAST:event_btnSAIR_AddPecasActionPerformed

    private void btnAdicionarPecaNovaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarPecaNovaActionPerformed
        adicionarPecaNova();
    }//GEN-LAST:event_btnAdicionarPecaNovaActionPerformed

    private void lblLGMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLGMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("LG");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblLGMouseClicked

    private void lblIphoneMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblIphoneMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("IPHONE");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblIphoneMouseClicked

    private void lblXiaomiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblXiaomiMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("XIAOMI");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblXiaomiMouseClicked

    private void lblLenovoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLenovoMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("LENOVO");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblLenovoMouseClicked

    private void lblAlcatelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAlcatelMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("ALCATEL");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblAlcatelMouseClicked

    private void lblAsusMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAsusMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("ASUS");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblAsusMouseClicked

    private void lblPositivoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPositivoMouseClicked
//        try {
        //p_orc.preencherMotorola();
        p_orc.preencherOrcamentos("POSITIVO");
//        } catch (SQLException e) {
//            JOptionPane.showMessageDialog(null, "Não foi possivel carregar a lista. \n\nERRO: " + e + "", "Erro ao Carregar", JOptionPane.ERROR_MESSAGE);
//        }
    }//GEN-LAST:event_lblPositivoMouseClicked

    private void btn_AddMotorolaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddMotorolaActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("MOTOROLA");
    }//GEN-LAST:event_btn_AddMotorolaActionPerformed

    private void btn_AddLGActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddLGActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("LG");
    }//GEN-LAST:event_btn_AddLGActionPerformed

    private void btn_AddIphoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddIphoneActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("IPHONE");
    }//GEN-LAST:event_btn_AddIphoneActionPerformed

    private void btn_AddXiaomiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddXiaomiActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("XIAOMI");
    }//GEN-LAST:event_btn_AddXiaomiActionPerformed

    private void btn_AddLenovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddLenovoActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("LENOVO");
    }//GEN-LAST:event_btn_AddLenovoActionPerformed

    private void btn_AddAlcatelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddAlcatelActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("ALCATEL");
    }//GEN-LAST:event_btn_AddAlcatelActionPerformed

    private void btn_AddAsusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddAsusActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("ASUS");
    }//GEN-LAST:event_btn_AddAsusActionPerformed

    private void btn_AddPositivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddPositivoActionPerformed
        jfr_Orc_AddPecas.setVisible(true);
        jfr_Orc_AddPecas.setLocationRelativeTo(null);
        txtMarcaAddPecas.setText("POSITIVO");
    }//GEN-LAST:event_btn_AddPositivoActionPerformed

    private void jtable_orcamentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtable_orcamentosMouseClicked
        int linha = jtable_orcamentos.getSelectedRow();
        int id = Integer.parseInt(jtable_orcamentos.getValueAt(linha, 0).toString());
        String Modelo = jtable_orcamentos.getValueAt(linha, 1).toString();
        if (evt.getClickCount() == 2) {
            int retorno = decisaoDeletar("Deseja deletar este item?");
            if (retorno == 0) {
                try {
                    d.deletarOrcamento(id);
                    // Atualizar lista.
                    JOptionPane.showMessageDialog(null, "Item deletado com sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
                    p_orc.preencherOrcamentos(Modelo);
                } catch (SQLException ex) {
                    Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_jtable_orcamentosMouseClicked

    private void lblXiaomiMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblXiaomiMousePressed
        jpanel_xiaomi.setBackground(new Color(0, 151, 169));
    }//GEN-LAST:event_lblXiaomiMousePressed

    private void lblXiaomiMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblXiaomiMouseReleased
        jpanel_xiaomi.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblXiaomiMouseReleased

    private void lblLenovoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLenovoMousePressed
        jpanel_lenovo.setBackground(new Color(207, 104, 150));
    }//GEN-LAST:event_lblLenovoMousePressed

    private void lblLenovoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLenovoMouseReleased
        jpanel_lenovo.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblLenovoMouseReleased

    private void lblAlcatelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAlcatelMousePressed
        jpanel_alcatel.setBackground(new Color(0, 109, 68));
    }//GEN-LAST:event_lblAlcatelMousePressed

    private void lblAlcatelMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAlcatelMouseReleased
        jpanel_alcatel.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblAlcatelMouseReleased

    private void lblAsusMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAsusMousePressed
        jpanel_asus.setBackground(new Color(188, 118, 97));
    }//GEN-LAST:event_lblAsusMousePressed

    private void lblAsusMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblAsusMouseReleased
        jpanel_asus.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblAsusMouseReleased

    private void lblPositivoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPositivoMousePressed
        jpanel_positivo.setBackground(new Color(154, 61, 222));
    }//GEN-LAST:event_lblPositivoMousePressed

    private void lblPositivoMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblPositivoMouseReleased
        jpanel_positivo.setBackground(new Color(40, 40, 40));
    }//GEN-LAST:event_lblPositivoMouseReleased

    private void txtLucroEditFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLucroEditFocusGained
        calcularLucro();

    }//GEN-LAST:event_txtLucroEditFocusGained

    private void txtProcurarClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProcurarClienteKeyPressed
        try {
            p.buscarCliente(txtProcurarCliente.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro ao pesquisar Cliente.\n\nERRO: " + e + "", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_txtProcurarClienteKeyPressed

    private void txtAparelhoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAparelhoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterOrdemServ();
        }
    }//GEN-LAST:event_txtAparelhoKeyPressed

    private void txtDefeitoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDefeitoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterOrdemServ();
        }
    }//GEN-LAST:event_txtDefeitoKeyPressed

    private void txtSenhaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterOrdemServ();
        }
    }//GEN-LAST:event_txtSenhaKeyPressed

    private void txtSituacaoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSituacaoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterOrdemServ();
        }
    }//GEN-LAST:event_txtSituacaoKeyPressed

    private void txtNomeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterRegistrarCliente();
        }
    }//GEN-LAST:event_txtNomeKeyPressed

    private void txtTelefoneKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefoneKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterRegistrarCliente();
        }
    }//GEN-LAST:event_txtTelefoneKeyPressed

    private void txtTelefoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefoneKeyTyped
        String caracteres = "0987654321-()";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
        txtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtTelefone.getText().length() <= 15 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtTelefoneKeyTyped

    private void txtPecaAddPecasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPecaAddPecasKeyTyped
        String caracteres = "0987654321.";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPecaAddPecasKeyTyped

    private void txtServicoAddPecasKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtServicoAddPecasKeyTyped
        String caracteres = "0987654321.";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtServicoAddPecasKeyTyped

    private void txtValorPecaEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorPecaEditKeyTyped
        String caracteres = "0987654321.";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtValorPecaEditKeyTyped

    private void txtValorServicoEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorServicoEditKeyTyped
        String caracteres = "0987654321.";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtValorServicoEditKeyTyped

    private void txtDataEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataEditKeyTyped
        String caracteres = "0987654321-";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtDataEditKeyTyped

    private void txtSenhaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaKeyTyped
        txtSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtSenha.getText().length() <= 10 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtSenhaKeyTyped

    private void txtNomeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeKeyTyped
        String caracteres = "0987654321-!@#$%^&*()_+=[]';.,`/|?\\☺☻♥♦♣♠•◘○";
        if (caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }

        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtNome.getText().length() <= 40 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtNomeKeyTyped

    private void txtAparelhoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAparelhoKeyTyped
        txtAparelho.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtAparelho.getText().length() <= 40 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtAparelhoKeyTyped

    private void txtDefeitoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDefeitoKeyTyped
        txtDefeito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtDefeito.getText().length() <= 200 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtDefeitoKeyTyped

    private void txtSituacaoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSituacaoKeyTyped
        txtSituacao.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtSituacao.getText().length() <= 500 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtSituacaoKeyTyped

    private void txtProcurarClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProcurarClienteKeyTyped
        String caracteres = "0987654321-!@#$%^&*()_+=[]';.,`/|?\\☺☻♥♦♣♠•◘○";
        if (caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }

        txtProcurarCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtProcurarCliente.getText().length() <= 40 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtProcurarClienteKeyTyped

    private void btnConcluirServicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConcluirServicoActionPerformed
        concluirServico();
    }//GEN-LAST:event_btnConcluirServicoActionPerformed

    public void concluirServico() {
        if (txtNomeEdit.getText().isEmpty() || txtAparelhoEdit.getText().isEmpty() || txtDefeitoEdit.getText().isEmpty() || txtPaneSituacaoEdit.getText().isEmpty()
                || txtValorPecaEdit.getText().isEmpty() || txtValorServicoEdit.getText().isEmpty() || txtLucroEdit.getText().isEmpty()
                || txtDataEdit.getText().isEmpty() || txtServicoEdit.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Preencha todos os campos especialmente\no ultimo campo 'Servico'.", "SUCESSO", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                //String data = simpleFormat.parse(txtDataEdit.getText());
                BigDecimal valor_servico = new BigDecimal(txtValorServicoEdit.getText());
                BigDecimal valor_peca = new BigDecimal(txtValorPecaEdit.getText());
                BigDecimal lucro = new BigDecimal(txtLucroEdit.getText());
                int ID = Integer.parseInt(lblSV_ID.getText());
                Date data = new java.sql.Date(((java.util.Date) simpleFormat.parse(txtDataEdit.getText())).getTime());

                a.concluirServicos(data, txtAparelhoEdit.getText(), txtDefeitoEdit.getText(), txtPaneSituacaoEdit.getText(), valor_servico, valor_peca, ID, txtSenhaEdit.getText(), lucro, txtServicoEdit.getText());
                p.preencherTableServicos();
                p.preencherTableGastosLucros();
                carregarLabelsGastosLucros();
                //lblLucrosServicosDone.setText(b.buscarLucrosServicosProntos().toString());
                jFrame1.dispose();
                JOptionPane.showMessageDialog(null, "Servico alterado com sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException | ClassNotFoundException | ParseException ex) {
                JOptionPane.showMessageDialog(null, "Erro ao alterar dados!\n\n" + ex, "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    private void txtModeloAddPEcasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtModeloAddPEcasKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtModeloAddPEcas.getText().isEmpty() || txtServicoAddPecas.getText().isEmpty() || txtPecaAddPecas.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Existem campos vazios, preencha todos!", "Campos Vazios", JOptionPane.WARNING_MESSAGE);
            } else {
                adicionarPecaNova();
            }
        }
    }//GEN-LAST:event_txtModeloAddPEcasKeyPressed

    private void txtServicoAddPecasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtServicoAddPecasKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtModeloAddPEcas.getText().isEmpty() || txtServicoAddPecas.getText().isEmpty() || txtPecaAddPecas.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Existem campos vazios, preencha todos!", "Campos Vazios", JOptionPane.WARNING_MESSAGE);
            } else {
                adicionarPecaNova();
            }
        }
    }//GEN-LAST:event_txtServicoAddPecasKeyPressed

    private void txtPecaAddPecasKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPecaAddPecasKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            if (txtModeloAddPEcas.getText().isEmpty() || txtServicoAddPecas.getText().isEmpty() || txtPecaAddPecas.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Existem campos vazios, preencha todos!", "Campos Vazios", JOptionPane.WARNING_MESSAGE);
            } else {
                adicionarPecaNova();
            }
        }
    }//GEN-LAST:event_txtPecaAddPecasKeyPressed

    private void jtable_GastosLucrosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtable_GastosLucrosMouseClicked
        int linha = jtable_GastosLucros.getSelectedRow();
        int id = Integer.parseInt(jtable_GastosLucros.getValueAt(linha, 10).toString());
        if (evt.getClickCount() == 2) {
            int retorno = decisaoDeletar("Deseja cancelar a conclusao deste servico?\nEle retonara para a tela de 'Ordens de Servico'");
            if (retorno == 0) {
                try {
                    a.desconcluirServicos(id);
                    // Atualizar lista.
                    preencherTodasTabelas();
                    //p.preencherTableGastosLucros();
                    JOptionPane.showMessageDialog(null, "Servico retornou com Sucesso para a aba 'Ordens de Servico'\nAperta F1 para retornar a primeira Aba", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception ex) {
                    Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_jtable_GastosLucrosMouseClicked

    private void btnSaveServicoEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveServicoEditActionPerformed
        teclaEnterServicoEdit();
    }//GEN-LAST:event_btnSaveServicoEditActionPerformed

    private void txtAparelhoEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAparelhoEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        }
    }//GEN-LAST:event_txtAparelhoEditKeyPressed

    private void txtDefeitoEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDefeitoEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        }
    }//GEN-LAST:event_txtDefeitoEditKeyPressed

    private void txtPaneSituacaoEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPaneSituacaoEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        } else if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            txtSenhaEdit.requestFocus();
        }
    }//GEN-LAST:event_txtPaneSituacaoEditKeyPressed

    private void txtSenhaEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        }
    }//GEN-LAST:event_txtSenhaEditKeyPressed

    private void txtValorPecaEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorPecaEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        }
    }//GEN-LAST:event_txtValorPecaEditKeyPressed

    private void txtValorServicoEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorServicoEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        }
    }//GEN-LAST:event_txtValorServicoEditKeyPressed

    private void txtDataEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDataEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterServicoEdit();
        }
    }//GEN-LAST:event_txtDataEditKeyPressed

    private void txtServicoEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtServicoEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int retorno = decisaoDeletar("Deseja mesmo concluir este serviço? ele será enviado\npara a tabela de 'Serviços Concluidos' - Atalho F3");
            if (retorno == 0) {
                concluirServico();
            }
        }
    }//GEN-LAST:event_txtServicoEditKeyPressed

    private void btnAddProdutoGastosPessoaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddProdutoGastosPessoaisActionPerformed
        inserirProduto();
    }//GEN-LAST:event_btnAddProdutoGastosPessoaisActionPerformed

    private void txtProdutoGastosPessoaisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProdutoGastosPessoaisKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            inserirProduto();
        }
    }//GEN-LAST:event_txtProdutoGastosPessoaisKeyPressed

    private void txtValorGastosPessoaisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorGastosPessoaisKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            inserirProduto();
        } else if (evt.getKeyCode() == KeyEvent.VK_COMMA) {
            JOptionPane.showMessageDialog(null, "Utilize ponto e nao virgula!");
        }
    }//GEN-LAST:event_txtValorGastosPessoaisKeyPressed

    private void jtable_GastosPessoaisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtable_GastosPessoaisMouseClicked
//        int linha = jtable_GastosPessoais.getSelectedRow();
//        int id = Integer.parseInt(jtable_GastosPessoais.getValueAt(linha, 0).toString());
//        if (evt.getClickCount() == 2) {
//            int retorno = decisaoDeletar("Deseja remover esse produto?");
//            if (retorno == 0) {
//                try {
//                    d.deletarProduto(id);
//                    // Atualizar lista.
//                    preencherTodasTabelas();
//                    JOptionPane.showMessageDialog(null, "Produto removido com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
//                } catch (Exception ex) {
//                    Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }//GEN-LAST:event_jtable_GastosPessoaisMouseClicked

    private void txtValorGastosPessoaisKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorGastosPessoaisKeyTyped
        // TODO add your handling code here:
        String caracteres = "0987654321.";
        String virgula = txtValorGastosPessoais.getText();
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
    }//GEN-LAST:event_txtValorGastosPessoaisKeyTyped

    private void txtProdutoGastosPessoaisKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProdutoGastosPessoaisKeyTyped
        String caracteres = "!@#$%^&*()_+=[]';.,`/|?\\☺☻♥♦♣♠•◘○";
        if (caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }

        txtProdutoGastosPessoais.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtProdutoGastosPessoais.getText().length() <= 40 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtProdutoGastosPessoaisKeyTyped

    private void txtLucroEditFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtLucroEditFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txtLucroEditFocusLost

    private void txtValorServicoEditKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValorServicoEditKeyReleased
        calcularLucro();
    }//GEN-LAST:event_txtValorServicoEditKeyReleased

    private void txtValorPecaEditFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtValorPecaEditFocusGained
        if (txtValorPecaEdit.getText().equals("0.00")) {

            txtValorPecaEdit.setText("");
        }
    }//GEN-LAST:event_txtValorPecaEditFocusGained

    private void txtValorServicoEditFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtValorServicoEditFocusGained
        if (txtValorServicoEdit.getText().equals("0.00")) {

            txtValorServicoEdit.setText("");
        }
    }//GEN-LAST:event_txtValorServicoEditFocusGained

    private void txtCPFKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCPFKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            teclaEnterRegistrarCliente();
        }
    }//GEN-LAST:event_txtCPFKeyPressed

    private void txtCPFKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCPFKeyTyped
        String caracteres = "0987654321-.";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
        txtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtTelefone.getText().length() <= 16 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtCPFKeyTyped

    private void btnSAIR_AddPecas1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_AddPecas1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnSAIR_AddPecas1ActionPerformed

    private void btnSAIR_AddPecas1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSAIR_AddPecas1MouseClicked
        jfr_AlterarCliente.dispose();
        txtNomeClienteEdit.setText("");
        txtCpfClienteEdit.setText("");
        txtTelefoneClienteEdit.setText("");
    }//GEN-LAST:event_btnSAIR_AddPecas1MouseClicked

    private void btnEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarClienteActionPerformed
        editarCliente();
        preencherTodasTabelas();
    }//GEN-LAST:event_btnEditarClienteActionPerformed

    private void txtCpfClienteEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCpfClienteEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            editarCliente();
            preencherTodasTabelas();
        }
    }//GEN-LAST:event_txtCpfClienteEditKeyPressed

    private void txtNomeClienteEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeClienteEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            editarCliente();
            preencherTodasTabelas();
        }
    }//GEN-LAST:event_txtNomeClienteEditKeyPressed

    private void txtTelefoneClienteEditKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefoneClienteEditKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            editarCliente();
            preencherTodasTabelas();
        }
    }//GEN-LAST:event_txtTelefoneClienteEditKeyPressed

    private void txtCpfClienteEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCpfClienteEditKeyTyped
        String caracteres = "0987654321-.";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
        txtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtTelefone.getText().length() <= 16 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtCpfClienteEditKeyTyped

    private void txtNomeClienteEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeClienteEditKeyTyped
        String caracteres = "0987654321-!@#$%^&*()_+=[]';.,`/|?\\☺☻♥♦♣♠•◘○";
        if (caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }

        txtNome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtNome.getText().length() <= 40 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtNomeClienteEditKeyTyped

    private void txtTelefoneClienteEditKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefoneClienteEditKeyTyped
        String caracteres = "0987654321-()";
        if (!caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        }
        txtTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent e) {
                int k = e.getKeyChar();
                if (txtTelefone.getText().length() <= 15 - 1) {
                    //deixe passar
                } else {
                    e.setKeyChar((char) KeyEvent.VK_CLEAR);
                }
            }
        });
    }//GEN-LAST:event_txtTelefoneClienteEditKeyTyped

    private void jtableOrdensServicosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtableOrdensServicosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int linha = 0;
//        try {
            linha = jtableOrdensServicos.getSelectedRow();
            //JOptionPane.showMessageDialog(null, "teste", "teste", JOptionPane.INFORMATION_MESSAGE);
            int id = Integer.parseInt(jtableOrdensServicos.getValueAt(linha, 9).toString());
            int retorno = decisaoDeletar("Deseja deletar este servico?");
            if (retorno == 0) {
                // try {
                d.deletarServicos(id);
                // Atualizar lista.
                p.preencherTableServicos();
//                    } catch (SQLException ex) {
//                        Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
//                    }
            }
        }
    }//GEN-LAST:event_jtableOrdensServicosKeyPressed

    private void jtableListaClientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtableListaClientesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int linha = 0;
            linha = jtableListaClientes.getSelectedRow();
            int id = Integer.parseInt(jtableListaClientes.getValueAt(linha, 0).toString());
            int retorno = decisaoDeletar("Deseja deletar este cliente?");
            if (retorno == 0) {
                try {
                    d.deletarClientes(id);
                    // Atualizar lista.
                    p.preencherRemoveClientes();
                    p.preencherComboboxCliente();
                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_jtableListaClientesKeyPressed

    private void jtable_GastosPessoaisKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jtable_GastosPessoaisKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_DELETE) {
            int linha = jtable_GastosPessoais.getSelectedRow();
            int id = Integer.parseInt(jtable_GastosPessoais.getValueAt(linha, 0).toString());
            int retorno = decisaoDeletar("Deseja remover esse produto?");
            if (retorno == 0) {
                try {
                    d.deletarProduto(id);
                    // Atualizar lista.
                    preencherTodasTabelas();
                    JOptionPane.showMessageDialog(null, "Produto removido com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception ex) {
                    Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_jtable_GastosPessoaisKeyPressed

    private void checkGastosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkGastosMouseClicked
        if (checkGastosTemp.isSelected()) {
            checkGastosTemp.setSelected(false);
        }
    }//GEN-LAST:event_checkGastosMouseClicked

    private void checkGastosTempMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkGastosTempMouseClicked
        if (checkGastos.isSelected()) {
            checkGastos.setSelected(false);
        }
    }//GEN-LAST:event_checkGastosTempMouseClicked

    private void jtable_GastosPessoaisTemporariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtable_GastosPessoaisTemporariosMouseClicked
        int linha = jtable_GastosPessoaisTemporarios.getSelectedRow();
        int id = Integer.parseInt(jtable_GastosPessoaisTemporarios.getValueAt(linha, 0).toString());
        if (evt.getClickCount() == 2) {
            int retorno = decisaoDeletar("Deseja passar esse produto para a tabela de Gastos?");
            if (retorno == 0) {
                try {
                    a.alterarGastoTemporario(id);
                    // Atualizar lista.
                    preencherTodasTabelas();
                    JOptionPane.showMessageDialog(null, "Produto transferido com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);
                } catch (Exception ex) {
                    Logger.getLogger(j_Principal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }//GEN-LAST:event_jtable_GastosPessoaisTemporariosMouseClicked

    private void btnSAIR_removeCl1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSAIR_removeCl1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSAIR_removeCl1ActionPerformed

    private void lblExtratoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblExtratoMouseClicked
        CardLayout cl = (CardLayout) CardLayout.getLayout();
        cl.show(CardLayout, "extrato");
        attStatus.atualizarStatusExtrato();
    }//GEN-LAST:event_lblExtratoMouseClicked

    private void btnVerExtratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerExtratoActionPerformed

//        System.out.println(date);
        if (jdateInicial.getDate() == null || jdateFinal.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Preencha os dois campos com a data inicial e final", "Datas Invalidas", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                String dataInicial = simpleFormat.format(jdateInicial.getDate());
                String dataFinal = simpleFormat.format(jdateFinal.getDate());
                BigDecimal lucExt = b.buscarLucrosExtrato(dataInicial, dataFinal);
                String lucroExtrato = b.buscarLucrosExtrato(dataInicial, dataFinal).toString();
                p.preencherTableExtrato(dataInicial, dataFinal);
                lblLucroExtrato.setText("R$ " + lucroExtrato);
                if (lucExt.signum() == -1) {
                    lblLucroExtrato.setForeground(Color.red);
                    Border lineBorder = BorderFactory.createLineBorder(Color.red, 3, true);
                    TitledBorder title = BorderFactory.createTitledBorder(lineBorder, "");
                    jpanelLucroExtrato.setBorder(title);
                } else if (lucExt.signum() == 0) {
                    lblLucroExtrato.setForeground(Color.white);
                    Border lineBorder = BorderFactory.createLineBorder(Color.white, 3, true);
                    TitledBorder title = BorderFactory.createTitledBorder(lineBorder, "");
                    jpanelLucroExtrato.setBorder(title);
                } else {
                    lblLucroExtrato.setForeground(Color.green);
                    Border lineBorder = BorderFactory.createLineBorder(Color.green, 3, true);
                    TitledBorder title = BorderFactory.createTitledBorder(lineBorder, "");
                    jpanelLucroExtrato.setBorder(title);
                }
            } catch (Exception e) {
            }
        }
    }//GEN-LAST:event_btnVerExtratoActionPerformed

    private void txtBuscarServicosClienteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarServicosClienteKeyTyped
        String caracteres = "0987654321-!@#$%^&*()_+=[]';.,`/|?\\☺☻♥♦♣♠•◘○";
        if (caracteres.contains(evt.getKeyChar() + "")) {
            evt.consume();
        } else if (txtBuscarServicosCliente.getText().equals("")) {
            preencherTodasTabelas();
        } else {
            b.buscarTodosServicosDeterminadoCliente(txtBuscarServicosCliente.getText());
        }
    }//GEN-LAST:event_txtBuscarServicosClienteKeyTyped

    private void txtBuscarServicosClienteKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarServicosClienteKeyPressed
        if (txtBuscarServicosCliente.getText().equals("")) {
            preencherTodasTabelas();
        }
    }//GEN-LAST:event_txtBuscarServicosClienteKeyPressed

    private void txtBuscarServicosClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscarServicosClienteKeyReleased
        if (txtBuscarServicosCliente.getText().equals("")) {
            preencherTodasTabelas();
        }
    }//GEN-LAST:event_txtBuscarServicosClienteKeyReleased

    private void lblLogoStandbyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblLogoStandbyMouseClicked
        int desejo1 = decisaoDeletar("Deseja reiniciar o mes?");
        if (desejo1 == 0) {
            int desejo2 = decisaoDeletar("Tem certeza que deseja reiniciar o mes?");
            if (desejo2 == 0) {
                int desejo3 = decisaoDeletar("Reiniciando o mes, a tabela de lucros vai ser absolutamente zerada\nvoce tem certeza disso?");
                if (desejo3 == 0) {
                    int desejo4 = decisaoDeletar("Ultimo Aviso! nao tem retorno! os dados vao ficar salvos no banco de dados\n"
                            + "mas eles nao vao ficar mais visiveis na tabela de lucros do mes, tem mesmo\n certeza disso?");
                    if (desejo4 == 0) {
                        a.reiniciarLucrosMes();
                        preencherTodasTabelas();
                    }
                }
            }
        }
    }//GEN-LAST:event_lblLogoStandbyMouseClicked

    public void editarCliente() {
        int id = Integer.parseInt(lblIdClienteEdit.getText());
        if (txtNomeClienteEdit.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "O Campo 'Nome' deve ser preenchido obrigatoriamente.", "Campos Vazios", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                a.alterarClientes(id, txtNomeClienteEdit.getText(), txtTelefoneClienteEdit.getText(), txtCpfClienteEdit.getText());
                JOptionPane.showMessageDialog(null, "Cliente alterado com sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Não foi possivel alterar o cliente.\n\nERRO: " + e + "", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void inserirProduto() {

        if (checkGastos.isSelected() || checkGastosTemp.isSelected() && txtProdutoGastosPessoais.getText().equals("") || txtValorGastosPessoais.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Voce deve preencher o campo 'Produto' e o campo 'Valor'", "AVISO!", JOptionPane.WARNING_MESSAGE);
        } else if (checkGastos.isSelected() && checkGastosTemp.isSelected()) {
            JOptionPane.showMessageDialog(null, "Marque apenas uma caixa!", "AVISO!", JOptionPane.WARNING_MESSAGE);
        } else if (!checkGastos.isSelected() && !checkGastosTemp.isSelected()) {
            JOptionPane.showMessageDialog(null, "Nenhuma caixa marcada, selecione uma!", "AVISO!", JOptionPane.WARNING_MESSAGE);
        } else if (checkGastos.isSelected()) {
            try {
                BigDecimal valorGastosPessoais = new BigDecimal(txtValorGastosPessoais.getText());
                i.inserirProduto(txtProdutoGastosPessoais.getText(), valorGastosPessoais, 0);
                txtProdutoGastosPessoais.setText("");
                txtValorGastosPessoais.setText("");
                txtProdutoGastosPessoais.requestFocus();
                preencherTodasTabelas();
            } catch (SQLException | ClassNotFoundException sqlError) {
                JOptionPane.showMessageDialog(null, "Nao foi possivel adicionar o produto, reinicie o sistema\n\nERRO: " + sqlError + "", "ERRO SQL", JOptionPane.ERROR_MESSAGE);
            }
        } else if (checkGastosTemp.isSelected()) {
            try {
                BigDecimal valorGastosPessoais = new BigDecimal(txtValorGastosPessoais.getText());
                i.inserirProduto(txtProdutoGastosPessoais.getText(), valorGastosPessoais, 1);
                txtProdutoGastosPessoais.setText("");
                txtValorGastosPessoais.setText("");
                txtProdutoGastosPessoais.requestFocus();
                preencherTodasTabelas();
            } catch (SQLException | ClassNotFoundException sqlError) {
                JOptionPane.showMessageDialog(null, "Nao foi possivel adicionar o produto, reinicie o sistema\n\nERRO: " + sqlError + "", "ERRO SQL", JOptionPane.ERROR_MESSAGE);
            }
        }
//        
//        try {
//            i.inserirProduto(txtProdutoGastosPessoais.getText(), valorGastosPessoais);
//            txtProdutoGastosPessoais.setText("");
//            txtValorGastosPessoais.setText("");
//            txtProdutoGastosPessoais.requestFocus();
//            preencherTodasTabelas();
//        } catch (SQLException | ClassNotFoundException sqlError) {
//            JOptionPane.showMessageDialog(null, "Nao foi possivel adicionar o produto, reinicie o sistema\n\nERRO: " + sqlError + "", "ERRO SQL", JOptionPane.ERROR_MESSAGE);
//        }
    }

    public void teclaEnterServicoEdit() {
        if (txtNomeEdit.getText().isEmpty() || txtAparelhoEdit.getText().isEmpty() || txtDefeitoEdit.getText().isEmpty() || txtPaneSituacaoEdit.getText().isEmpty()
                || txtValorPecaEdit.getText().isEmpty() || txtValorServicoEdit.getText().isEmpty() || txtLucroEdit.getText().isEmpty()
                || txtDataEdit.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Todos os campos exceto o ultimo sao necessarios!\nFavor preenche-los.", "CAMPOS", JOptionPane.WARNING_MESSAGE);
        } else {
            try {
                //String data = simpleFormat.parse(txtDataEdit.getText());
                BigDecimal valor_servico = new BigDecimal(txtValorServicoEdit.getText());
                BigDecimal valor_peca = new BigDecimal(txtValorPecaEdit.getText());
                BigDecimal lucro = new BigDecimal(txtLucroEdit.getText());
                int ID = Integer.parseInt(lblSV_ID.getText());
                Date data = new java.sql.Date(((java.util.Date) simpleFormat.parse(txtDataEdit.getText())).getTime());

                a.alterarServicos(data, txtAparelhoEdit.getText(), txtDefeitoEdit.getText(), txtPaneSituacaoEdit.getText(), valor_servico, valor_peca, ID, txtSenhaEdit.getText(), lucro);
                p.preencherTableServicos();
                //p.preencherTableLucros();
                jFrame1.dispose();
                limparCamposEdicaoServico();
                JOptionPane.showMessageDialog(this, "Servico alterado com sucesso!", "SUCESSO", JOptionPane.INFORMATION_MESSAGE);
            } catch (SQLException | ClassNotFoundException | ParseException ex) {
                JOptionPane.showMessageDialog(this, "Erro ao alterar dados!\n\n" + ex, "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(j_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(j_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(j_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(j_Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new j_Principal().setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JPanel CardLayout;
    private javax.swing.JPanel atras_aparelho;
    private javax.swing.JPanel atras_cliente;
    private javax.swing.JTextField atras_clienteCOMBO;
    private javax.swing.JPanel atras_defeito;
    private javax.swing.JPanel atras_situacao;
    private javax.swing.JPanel atras_valor;
    private javax.swing.JPanel bg_orcamento;
    private javax.swing.JButton btnAddProdutoGastosPessoais;
    private javax.swing.JButton btnAdicionarPecaNova;
    private javax.swing.JButton btnConcluirServico;
    private javax.swing.JButton btnEditarCliente;
    private javax.swing.JButton btnRegistrarCliente;
    private javax.swing.JButton btnSAIR_AddPecas;
    private javax.swing.JButton btnSAIR_AddPecas1;
    private javax.swing.JButton btnSAIR_cliente;
    private javax.swing.JButton btnSAIR_form1;
    private javax.swing.JButton btnSAIR_form2;
    private javax.swing.JButton btnSAIR_ordens;
    private javax.swing.JButton btnSAIR_removeCl;
    private javax.swing.JButton btnSAIR_removeCl1;
    private javax.swing.JButton btnSaveServicoEdit;
    private javax.swing.JButton btnVerExtrato;
    private javax.swing.JButton btn_AddAlcatel;
    private javax.swing.JButton btn_AddAsus;
    private javax.swing.JButton btn_AddIphone;
    private javax.swing.JButton btn_AddLG;
    private javax.swing.JButton btn_AddLenovo;
    private javax.swing.JButton btn_AddMotorola;
    private javax.swing.JButton btn_AddPositivo;
    private javax.swing.JButton btn_AddSamsung;
    private javax.swing.JButton btn_AddXiaomi;
    private javax.swing.JCheckBox checkGastos;
    private javax.swing.JCheckBox checkGastosTemp;
    private javax.swing.JPanel divisoria;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    public static javax.swing.JComboBox jcmbNomeCliente;
    private com.toedter.calendar.JDateChooser jdateFinal;
    private com.toedter.calendar.JDateChooser jdateInicial;
    private javax.swing.JFrame jfr_AlterarCliente;
    private javax.swing.JFrame jfr_Orc_AddPecas;
    private javax.swing.JFrame jfr_Orcamento;
    private javax.swing.JPanel jpaneGastos;
    private javax.swing.JPanel jpanelLucroExtrato;
    private javax.swing.JPanel jpanel_LG;
    private javax.swing.JPanel jpanel_alcatel;
    private javax.swing.JPanel jpanel_asus;
    private javax.swing.JPanel jpanel_iphone;
    private javax.swing.JPanel jpanel_lenovo;
    private javax.swing.JPanel jpanel_motorola;
    private javax.swing.JPanel jpanel_positivo;
    private javax.swing.JPanel jpanel_samsung;
    private javax.swing.JPanel jpanel_xiaomi;
    public static javax.swing.JTable jtableListaClientes;
    public static javax.swing.JTable jtableOrdensServicos;
    public static javax.swing.JTable jtable_Extrato;
    public static javax.swing.JTable jtable_GastosLucros;
    public static javax.swing.JTable jtable_GastosPessoais;
    public static javax.swing.JTable jtable_GastosPessoaisTemporarios;
    public static javax.swing.JTable jtable_orcamentos;
    private javax.swing.JLabel lblAddEntrada;
    private javax.swing.JLabel lblAlcatel;
    private javax.swing.JLabel lblAparelho;
    private javax.swing.JLabel lblAparelhoEdit;
    private javax.swing.JLabel lblAsus;
    private javax.swing.JLabel lblCPF;
    private javax.swing.JLabel lblCad_Cliente;
    private javax.swing.JLabel lblCliente;
    private javax.swing.JLabel lblDataEdit;
    private javax.swing.JLabel lblDefeito;
    private javax.swing.JLabel lblDefeitoEdit;
    private javax.swing.JLabel lblExtrato;
    private javax.swing.JLabel lblGastosPessoais;
    private javax.swing.JLabel lblGastosPessoaisText;
    private javax.swing.JLabel lblIdClienteEdit;
    private javax.swing.JLabel lblIphone;
    private javax.swing.JLabel lblLG;
    private javax.swing.JLabel lblLenovo;
    private javax.swing.JLabel lblLogoStandby;
    private javax.swing.JLabel lblLucroEdit;
    private javax.swing.JLabel lblLucroExtrato;
    private javax.swing.JLabel lblLucroServicosText;
    private javax.swing.JLabel lblLucroTotal;
    private javax.swing.JLabel lblLucroTotalText;
    private javax.swing.JLabel lblLucrosServicosDone;
    private javax.swing.JLabel lblModeloAddPecas;
    private javax.swing.JLabel lblModeloAddPecas1;
    private javax.swing.JLabel lblMotorola;
    private javax.swing.JLabel lblNome;
    private javax.swing.JLabel lblNomeEdit;
    private javax.swing.JLabel lblOrcamento;
    private javax.swing.JLabel lblOrdensServico;
    private javax.swing.JLabel lblOrdensTOPMENU;
    private javax.swing.JLabel lblOrdensTOPMENU1;
    private javax.swing.JLabel lblOrdensTOPMENU2;
    private javax.swing.JLabel lblOrdensTOPMENU3;
    private javax.swing.JLabel lblOrdensTOPMENU5;
    private javax.swing.JLabel lblPecaAddPecas;
    private javax.swing.JLabel lblPecaAddPecas1;
    private javax.swing.JLabel lblPositivo;
    private javax.swing.JLabel lblProdutoGastosPessoais;
    private javax.swing.JLabel lblRemoveEntrada;
    private javax.swing.JLabel lblSV_ID;
    private javax.swing.JLabel lblSamsung;
    private javax.swing.JLabel lblSenhaEdit;
    private javax.swing.JLabel lblServicoAddPecas;
    private javax.swing.JLabel lblServicoAddPecas1;
    private javax.swing.JLabel lblServicoEdit;
    private javax.swing.JLabel lblServicos_Done;
    private javax.swing.JLabel lblSituacao;
    private javax.swing.JLabel lblSituacaoEdit;
    private javax.swing.JLabel lblTelefone;
    private javax.swing.JLabel lblValor;
    private javax.swing.JLabel lblValorGastosPessoais;
    private javax.swing.JLabel lblValorPecaEdit;
    private javax.swing.JLabel lblValorServicoEdit;
    private javax.swing.JLabel lblVoltar;
    private javax.swing.JLabel lblXiaomi;
    public static javax.swing.JPanel menu1_service;
    public static javax.swing.JPanel menu2_register;
    public static javax.swing.JPanel menu3_doneServices;
    public static javax.swing.JPanel menu4_orcament;
    public static javax.swing.JPanel menu5_extrato;
    private javax.swing.JPanel pane_Add_Entrada;
    private javax.swing.JPanel pane_Cad_Cliente;
    private javax.swing.JPanel pane_Extrato;
    private javax.swing.JPanel pane_GastosLucros;
    private javax.swing.JPanel pane_Menu;
    private javax.swing.JPanel pane_Ordens;
    private javax.swing.JPanel pane_Remove_Entrada;
    private javax.swing.JPanel pane_Servicos;
    private javax.swing.JPanel pane_Servicos1;
    private javax.swing.JPanel pane_Servicos2;
    private javax.swing.JPanel pane_Servicos3;
    private javax.swing.JPanel pane_Servicos4;
    private javax.swing.JPanel pane_TopSide1;
    private javax.swing.JPanel pane_TopSide2;
    private javax.swing.JPanel pane_form1;
    private javax.swing.JPanel pane_form2;
    private javax.swing.JPanel pane_form3;
    private javax.swing.JSeparator separador1;
    private javax.swing.JSeparator separador2;
    private javax.swing.JSeparator separador3;
    private javax.swing.JSeparator separador4;
    private javax.swing.JPanel topside;
    private javax.swing.JPanel topside_orcamento;
    private javax.swing.JTextField txtAparelho;
    private javax.swing.JTextField txtAparelhoEdit;
    private javax.swing.JTextField txtBuscarServicosCliente;
    private javax.swing.JFormattedTextField txtCPF;
    private javax.swing.JFormattedTextField txtCpfClienteEdit;
    private javax.swing.JTextField txtDataEdit;
    private javax.swing.JTextField txtDefeito;
    private javax.swing.JTextField txtDefeitoEdit;
    private javax.swing.JTextField txtLucroEdit;
    private javax.swing.JTextField txtMarcaAddPecas;
    private javax.swing.JTextField txtModeloAddPEcas;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtNomeClienteEdit;
    private javax.swing.JTextField txtNomeEdit;
    private javax.swing.JTextPane txtPaneSituacaoEdit;
    private javax.swing.JTextField txtPecaAddPecas;
    private javax.swing.JTextField txtProcurarCliente;
    private javax.swing.JTextField txtProdutoGastosPessoais;
    private javax.swing.JTextField txtSenha;
    private javax.swing.JTextField txtSenhaEdit;
    private javax.swing.JTextField txtServicoAddPecas;
    private javax.swing.JTextField txtServicoEdit;
    private javax.swing.JTextField txtSituacao;
    private javax.swing.JTextField txtTelefone;
    private javax.swing.JTextField txtTelefoneClienteEdit;
    private javax.swing.JTextField txtValorGastosPessoais;
    private javax.swing.JTextField txtValorPecaEdit;
    private javax.swing.JTextField txtValorServicoEdit;
    // End of variables declaration//GEN-END:variables
}
